package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.DemoUser;

import java.util.List;

/**
 * @author 11752
 * @version 1.0
 * @date 2021/6/8 14:36
 */
public interface DemoPgMapper {

    List<DemoUser> listDemo();
}