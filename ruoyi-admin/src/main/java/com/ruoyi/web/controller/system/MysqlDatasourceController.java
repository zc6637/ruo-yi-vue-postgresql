package com.ruoyi.web.controller.system;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.ruoyi.system.domain.DemoUser;
import com.ruoyi.system.mapper.DemoMysqlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.sql.SQLOutput;
import java.util.List;

/**
 * @author 11752
 * @version 1.0
 * @date 2021/6/8 14:24
 */
@DS("slave_1")
@RestController
public class MysqlDatasourceController {

    @Autowired
    private DemoMysqlMapper demoMysqlMapper;

    @GetMapping("/info")
    public List<DemoUser> info() {
        return demoMysqlMapper.listDemo();
    }

}