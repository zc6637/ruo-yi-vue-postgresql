/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1-pg
 Source Server Type    : PostgreSQL
 Source Server Version : 110012
 Source Host           : localhost:5432
 Source Catalog        : ry-vue
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110012
 File Encoding         : 65001

 Date: 08/06/2021 16:06:09
*/


-- ----------------------------
-- Sequence structure for column_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."column_id_seq";
CREATE SEQUENCE "public"."column_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for config_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."config_id_seq";
CREATE SEQUENCE "public"."config_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 4
CACHE 1;

-- ----------------------------
-- Sequence structure for dept_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."dept_id_seq";
CREATE SEQUENCE "public"."dept_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 110
CACHE 1;

-- ----------------------------
-- Sequence structure for dict_code_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."dict_code_seq";
CREATE SEQUENCE "public"."dict_code_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 29
CACHE 1;

-- ----------------------------
-- Sequence structure for dict_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."dict_id_seq";
CREATE SEQUENCE "public"."dict_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 11
CACHE 1;

-- ----------------------------
-- Sequence structure for info_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."info_id_seq";
CREATE SEQUENCE "public"."info_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 4
CACHE 1;

-- ----------------------------
-- Sequence structure for job_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."job_id_seq";
CREATE SEQUENCE "public"."job_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 4
CACHE 1;

-- ----------------------------
-- Sequence structure for job_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."job_log_id_seq";
CREATE SEQUENCE "public"."job_log_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for menu_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."menu_id_seq";
CREATE SEQUENCE "public"."menu_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1061
CACHE 1;

-- ----------------------------
-- Sequence structure for notice_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."notice_id_seq";
CREATE SEQUENCE "public"."notice_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 3
CACHE 1;

-- ----------------------------
-- Sequence structure for oper_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."oper_id_seq";
CREATE SEQUENCE "public"."oper_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_id_seq";
CREATE SEQUENCE "public"."post_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 5
CACHE 1;

-- ----------------------------
-- Sequence structure for role_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."role_id_seq";
CREATE SEQUENCE "public"."role_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 3
CACHE 1;

-- ----------------------------
-- Sequence structure for table_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."table_id_seq";
CREATE SEQUENCE "public"."table_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_id_seq";
CREATE SEQUENCE "public"."user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 3
CACHE 1;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS "public"."gen_table";
CREATE TABLE "public"."gen_table" (
  "table_id" int8 NOT NULL DEFAULT nextval('table_id_seq'::regclass),
  "table_name" varchar(200) COLLATE "pg_catalog"."default",
  "table_comment" varchar(500) COLLATE "pg_catalog"."default",
  "sub_table_name" varchar(64) COLLATE "pg_catalog"."default",
  "sub_table_fk_name" varchar(64) COLLATE "pg_catalog"."default",
  "class_name" varchar(100) COLLATE "pg_catalog"."default",
  "tpl_category" varchar(200) COLLATE "pg_catalog"."default" DEFAULT 'crud'::character varying,
  "package_name" varchar(100) COLLATE "pg_catalog"."default",
  "module_name" varchar(30) COLLATE "pg_catalog"."default",
  "business_name" varchar(30) COLLATE "pg_catalog"."default",
  "function_name" varchar(50) COLLATE "pg_catalog"."default",
  "function_author" varchar(50) COLLATE "pg_catalog"."default",
  "gen_type" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "gen_path" varchar(200) COLLATE "pg_catalog"."default" DEFAULT '/'::character varying,
  "options" varchar(1000) COLLATE "pg_catalog"."default",
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."gen_table"."table_id" IS '编号';
COMMENT ON COLUMN "public"."gen_table"."table_name" IS '表名称';
COMMENT ON COLUMN "public"."gen_table"."table_comment" IS '表描述';
COMMENT ON COLUMN "public"."gen_table"."sub_table_name" IS '关联子表的表名';
COMMENT ON COLUMN "public"."gen_table"."sub_table_fk_name" IS '子表关联的外键名';
COMMENT ON COLUMN "public"."gen_table"."class_name" IS '实体类名称';
COMMENT ON COLUMN "public"."gen_table"."tpl_category" IS '使用的模板（crud单表操作 tree树表操作）';
COMMENT ON COLUMN "public"."gen_table"."package_name" IS '生成包路径';
COMMENT ON COLUMN "public"."gen_table"."module_name" IS '生成模块名';
COMMENT ON COLUMN "public"."gen_table"."business_name" IS '生成业务名';
COMMENT ON COLUMN "public"."gen_table"."function_name" IS '生成功能名';
COMMENT ON COLUMN "public"."gen_table"."function_author" IS '生成功能作者';
COMMENT ON COLUMN "public"."gen_table"."gen_type" IS '生成代码方式（0zip压缩包 1自定义路径）';
COMMENT ON COLUMN "public"."gen_table"."gen_path" IS '生成路径（不填默认项目路径）';
COMMENT ON COLUMN "public"."gen_table"."options" IS '其它生成选项';
COMMENT ON COLUMN "public"."gen_table"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."gen_table"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."gen_table"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."gen_table"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."gen_table"."remark" IS '备注';
COMMENT ON TABLE "public"."gen_table" IS '代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS "public"."gen_table_column";
CREATE TABLE "public"."gen_table_column" (
  "column_id" int8 NOT NULL DEFAULT nextval('column_id_seq'::regclass),
  "table_id" varchar(64) COLLATE "pg_catalog"."default",
  "column_name" varchar(200) COLLATE "pg_catalog"."default",
  "column_comment" varchar(500) COLLATE "pg_catalog"."default",
  "column_type" varchar(100) COLLATE "pg_catalog"."default",
  "java_type" varchar(500) COLLATE "pg_catalog"."default",
  "java_field" varchar(200) COLLATE "pg_catalog"."default",
  "is_pk" char(1) COLLATE "pg_catalog"."default",
  "is_increment" char(1) COLLATE "pg_catalog"."default",
  "is_required" char(1) COLLATE "pg_catalog"."default",
  "is_insert" char(1) COLLATE "pg_catalog"."default",
  "is_edit" char(1) COLLATE "pg_catalog"."default",
  "is_list" char(1) COLLATE "pg_catalog"."default",
  "is_query" char(1) COLLATE "pg_catalog"."default",
  "query_type" varchar(200) COLLATE "pg_catalog"."default" DEFAULT 'EQ'::character varying,
  "html_type" varchar(200) COLLATE "pg_catalog"."default",
  "dict_type" varchar(200) COLLATE "pg_catalog"."default",
  "sort" int4,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."gen_table_column"."column_id" IS '编号';
COMMENT ON COLUMN "public"."gen_table_column"."table_id" IS '归属表编号';
COMMENT ON COLUMN "public"."gen_table_column"."column_name" IS '列名称';
COMMENT ON COLUMN "public"."gen_table_column"."column_comment" IS '列描述';
COMMENT ON COLUMN "public"."gen_table_column"."column_type" IS '列类型';
COMMENT ON COLUMN "public"."gen_table_column"."java_type" IS 'JAVA类型';
COMMENT ON COLUMN "public"."gen_table_column"."java_field" IS 'JAVA字段名';
COMMENT ON COLUMN "public"."gen_table_column"."is_pk" IS '是否主键（1是）';
COMMENT ON COLUMN "public"."gen_table_column"."is_increment" IS '是否自增（1是）';
COMMENT ON COLUMN "public"."gen_table_column"."is_required" IS '是否必填（1是）';
COMMENT ON COLUMN "public"."gen_table_column"."is_insert" IS '是否为插入字段（1是）';
COMMENT ON COLUMN "public"."gen_table_column"."is_edit" IS '是否编辑字段（1是）';
COMMENT ON COLUMN "public"."gen_table_column"."is_list" IS '是否列表字段（1是）';
COMMENT ON COLUMN "public"."gen_table_column"."is_query" IS '是否查询字段（1是）';
COMMENT ON COLUMN "public"."gen_table_column"."query_type" IS '查询方式（等于、不等于、大于、小于、范围）';
COMMENT ON COLUMN "public"."gen_table_column"."html_type" IS '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）';
COMMENT ON COLUMN "public"."gen_table_column"."dict_type" IS '字典类型';
COMMENT ON COLUMN "public"."gen_table_column"."sort" IS '排序';
COMMENT ON COLUMN "public"."gen_table_column"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."gen_table_column"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."gen_table_column"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."gen_table_column"."update_time" IS '更新时间';
COMMENT ON TABLE "public"."gen_table_column" IS '代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_blob_triggers";
CREATE TABLE "public"."qrtz_blob_triggers" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "blob_data" bytea
)
;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_calendars";
CREATE TABLE "public"."qrtz_calendars" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "calendar_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "calendar" bytea NOT NULL
)
;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_cron_triggers";
CREATE TABLE "public"."qrtz_cron_triggers" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "cron_expression" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "time_zone_id" varchar(80) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO "public"."qrtz_cron_triggers" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO "public"."qrtz_cron_triggers" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO "public"."qrtz_cron_triggers" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_fired_triggers";
CREATE TABLE "public"."qrtz_fired_triggers" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "entry_id" varchar(95) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "instance_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "fired_time" int8 NOT NULL,
  "sched_time" int8 NOT NULL,
  "priority" int4 NOT NULL,
  "state" varchar(16) COLLATE "pg_catalog"."default" NOT NULL,
  "job_name" varchar(200) COLLATE "pg_catalog"."default",
  "job_group" varchar(200) COLLATE "pg_catalog"."default",
  "is_nonconcurrent" bool,
  "requests_recovery" bool
)
;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_job_details";
CREATE TABLE "public"."qrtz_job_details" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "job_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "job_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(250) COLLATE "pg_catalog"."default",
  "job_class_name" varchar(250) COLLATE "pg_catalog"."default" NOT NULL,
  "is_durable" bool NOT NULL,
  "is_nonconcurrent" bool NOT NULL,
  "is_update_data" bool NOT NULL,
  "requests_recovery" bool NOT NULL,
  "job_data" bytea
)
;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO "public"."qrtz_job_details" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', 'f', 't', 'f', 'f', E'\\254\\355\\000\\005sr\\000\\025org.quartz.JobDataMap\\237\\260\\203\\350\\277\\251\\260\\313\\002\\000\\000xr\\000&org.quartz.utils.StringKeyDirtyFlagMap\\202\\010\\350\\303\\373\\305](\\002\\000\\001Z\\000\\023allowsTransientDataxr\\000\\035org.quartz.utils.DirtyFlagMap\\023\\346.\\255(v\\012\\316\\002\\000\\002Z\\000\\005dirtyL\\000\\003mapt\\000\\017Ljava/util/Map;xp\\001sr\\000\\021java.util.HashMap\\005\\007\\332\\301\\303\\026`\\321\\003\\000\\002F\\000\\012loadFactorI\\000\\011thresholdxp?@\\000\\000\\000\\000\\000\\014w\\010\\000\\000\\000\\020\\000\\000\\000\\001t\\000\\017TASK_PROPERTIESsr\\000\\036com.ruoyi.quartz.domain.SysJob\\000\\000\\000\\000\\000\\000\\000\\001\\002\\000\\010L\\000\\012concurrentt\\000\\022Ljava/lang/String;L\\000\\016cronExpressionq\\000~\\000\\011L\\000\\014invokeTargetq\\000~\\000\\011L\\000\\010jobGroupq\\000~\\000\\011L\\000\\005jobIdt\\000\\020Ljava/lang/Long;L\\000\\007jobNameq\\000~\\000\\011L\\000\\015misfirePolicyq\\000~\\000\\011L\\000\\006statusq\\000~\\000\\011xr\\000''com.ruoyi.common.core.domain.BaseEntity\\000\\000\\000\\000\\000\\000\\000\\001\\002\\000\\007L\\000\\010createByq\\000~\\000\\011L\\000\\012createTimet\\000\\020Ljava/util/Date;L\\000\\006paramsq\\000~\\000\\003L\\000\\006remarkq\\000~\\000\\011L\\000\\013searchValueq\\000~\\000\\011L\\000\\010updateByq\\000~\\000\\011L\\000\\012updateTimeq\\000~\\000\\014xpt\\000\\005adminsr\\000\\016java.util.Datehj\\201\\001KYt\\031\\003\\000\\000xpw\\010\\000\\000\\001y\\325\\230\\306\\360xpt\\000\\000pppt\\000\\0011t\\000\\0160/15 * * * * ?t\\000\\025ryTask.ryParams(''ry'')t\\000\\007DEFAULTsr\\000\\016java.lang.Long;\\213\\344\\220\\314\\217#\\337\\002\\000\\001J\\000\\005valuexr\\000\\020java.lang.Number\\206\\254\\225\\035\\013\\224\\340\\213\\002\\000\\000xp\\000\\000\\000\\000\\000\\000\\000\\002t\\000\\030\\347\\263\\273\\347\\273\\237\\351\\273\\230\\350\\256\\244\\357\\274\\210\\346\\234\\211\\345\\217\\202\\357\\274\\211t\\000\\0013t\\000\\0011x\\000');
INSERT INTO "public"."qrtz_job_details" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', 'f', 't', 'f', 'f', E'\\254\\355\\000\\005sr\\000\\025org.quartz.JobDataMap\\237\\260\\203\\350\\277\\251\\260\\313\\002\\000\\000xr\\000&org.quartz.utils.StringKeyDirtyFlagMap\\202\\010\\350\\303\\373\\305](\\002\\000\\001Z\\000\\023allowsTransientDataxr\\000\\035org.quartz.utils.DirtyFlagMap\\023\\346.\\255(v\\012\\316\\002\\000\\002Z\\000\\005dirtyL\\000\\003mapt\\000\\017Ljava/util/Map;xp\\001sr\\000\\021java.util.HashMap\\005\\007\\332\\301\\303\\026`\\321\\003\\000\\002F\\000\\012loadFactorI\\000\\011thresholdxp?@\\000\\000\\000\\000\\000\\014w\\010\\000\\000\\000\\020\\000\\000\\000\\001t\\000\\017TASK_PROPERTIESsr\\000\\036com.ruoyi.quartz.domain.SysJob\\000\\000\\000\\000\\000\\000\\000\\001\\002\\000\\010L\\000\\012concurrentt\\000\\022Ljava/lang/String;L\\000\\016cronExpressionq\\000~\\000\\011L\\000\\014invokeTargetq\\000~\\000\\011L\\000\\010jobGroupq\\000~\\000\\011L\\000\\005jobIdt\\000\\020Ljava/lang/Long;L\\000\\007jobNameq\\000~\\000\\011L\\000\\015misfirePolicyq\\000~\\000\\011L\\000\\006statusq\\000~\\000\\011xr\\000''com.ruoyi.common.core.domain.BaseEntity\\000\\000\\000\\000\\000\\000\\000\\001\\002\\000\\007L\\000\\010createByq\\000~\\000\\011L\\000\\012createTimet\\000\\020Ljava/util/Date;L\\000\\006paramsq\\000~\\000\\003L\\000\\006remarkq\\000~\\000\\011L\\000\\013searchValueq\\000~\\000\\011L\\000\\010updateByq\\000~\\000\\011L\\000\\012updateTimeq\\000~\\000\\014xpt\\000\\005adminsr\\000\\016java.util.Datehj\\201\\001KYt\\031\\003\\000\\000xpw\\010\\000\\000\\001y\\325\\230\\306\\360xpt\\000\\000pppt\\000\\0011t\\000\\0160/20 * * * * ?t\\0008ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)t\\000\\007DEFAULTsr\\000\\016java.lang.Long;\\213\\344\\220\\314\\217#\\337\\002\\000\\001J\\000\\005valuexr\\000\\020java.lang.Number\\206\\254\\225\\035\\013\\224\\340\\213\\002\\000\\000xp\\000\\000\\000\\000\\000\\000\\000\\003t\\000\\030\\347\\263\\273\\347\\273\\237\\351\\273\\230\\350\\256\\244\\357\\274\\210\\345\\244\\232\\345\\217\\202\\357\\274\\211t\\000\\0013t\\000\\0011x\\000');
INSERT INTO "public"."qrtz_job_details" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', 'f', 't', 'f', 'f', E'\\254\\355\\000\\005sr\\000\\025org.quartz.JobDataMap\\237\\260\\203\\350\\277\\251\\260\\313\\002\\000\\000xr\\000&org.quartz.utils.StringKeyDirtyFlagMap\\202\\010\\350\\303\\373\\305](\\002\\000\\001Z\\000\\023allowsTransientDataxr\\000\\035org.quartz.utils.DirtyFlagMap\\023\\346.\\255(v\\012\\316\\002\\000\\002Z\\000\\005dirtyL\\000\\003mapt\\000\\017Ljava/util/Map;xp\\001sr\\000\\021java.util.HashMap\\005\\007\\332\\301\\303\\026`\\321\\003\\000\\002F\\000\\012loadFactorI\\000\\011thresholdxp?@\\000\\000\\000\\000\\000\\014w\\010\\000\\000\\000\\020\\000\\000\\000\\001t\\000\\017TASK_PROPERTIESsr\\000\\036com.ruoyi.quartz.domain.SysJob\\000\\000\\000\\000\\000\\000\\000\\001\\002\\000\\010L\\000\\012concurrentt\\000\\022Ljava/lang/String;L\\000\\016cronExpressionq\\000~\\000\\011L\\000\\014invokeTargetq\\000~\\000\\011L\\000\\010jobGroupq\\000~\\000\\011L\\000\\005jobIdt\\000\\020Ljava/lang/Long;L\\000\\007jobNameq\\000~\\000\\011L\\000\\015misfirePolicyq\\000~\\000\\011L\\000\\006statusq\\000~\\000\\011xr\\000''com.ruoyi.common.core.domain.BaseEntity\\000\\000\\000\\000\\000\\000\\000\\001\\002\\000\\007L\\000\\010createByq\\000~\\000\\011L\\000\\012createTimet\\000\\020Ljava/util/Date;L\\000\\006paramsq\\000~\\000\\003L\\000\\006remarkq\\000~\\000\\011L\\000\\013searchValueq\\000~\\000\\011L\\000\\010updateByq\\000~\\000\\011L\\000\\012updateTimeq\\000~\\000\\014xpt\\000\\005adminsr\\000\\016java.util.Datehj\\201\\001KYt\\031\\003\\000\\000xpw\\010\\000\\000\\001y\\325\\230\\306\\360xpt\\000\\000pppt\\000\\0011t\\000\\0160/10 * * * * ?t\\000\\021ryTask.ryNoParamst\\000\\007DEFAULTsr\\000\\016java.lang.Long;\\213\\344\\220\\314\\217#\\337\\002\\000\\001J\\000\\005valuexr\\000\\020java.lang.Number\\206\\254\\225\\035\\013\\224\\340\\213\\002\\000\\000xp\\000\\000\\000\\000\\000\\000\\000\\001t\\000\\030\\347\\263\\273\\347\\273\\237\\351\\273\\230\\350\\256\\244\\357\\274\\210\\346\\227\\240\\345\\217\\202\\357\\274\\211t\\000\\0013t\\000\\0011x\\000');

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_locks";
CREATE TABLE "public"."qrtz_locks" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "lock_name" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO "public"."qrtz_locks" VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');
INSERT INTO "public"."qrtz_locks" VALUES ('RuoyiScheduler', 'STATE_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_paused_trigger_grps";
CREATE TABLE "public"."qrtz_paused_trigger_grps" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_scheduler_state";
CREATE TABLE "public"."qrtz_scheduler_state" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "instance_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "last_checkin_time" int8 NOT NULL,
  "checkin_interval" int8 NOT NULL
)
;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO "public"."qrtz_scheduler_state" VALUES ('RuoyiScheduler', 'localhost.localdomain1623138433830', 1623139562230, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_simple_triggers";
CREATE TABLE "public"."qrtz_simple_triggers" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "repeat_count" int8 NOT NULL,
  "repeat_interval" int8 NOT NULL,
  "times_triggered" int8 NOT NULL
)
;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_simprop_triggers";
CREATE TABLE "public"."qrtz_simprop_triggers" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "str_prop_1" varchar(512) COLLATE "pg_catalog"."default",
  "str_prop_2" varchar(512) COLLATE "pg_catalog"."default",
  "str_prop_3" varchar(512) COLLATE "pg_catalog"."default",
  "int_prop_1" int4,
  "int_prop_2" int4,
  "long_prop_1" int8,
  "long_prop_2" int8,
  "dec_prop_1" numeric(13,4),
  "dec_prop_2" numeric(13,4),
  "bool_prop_1" bool,
  "bool_prop_2" bool
)
;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS "public"."qrtz_triggers";
CREATE TABLE "public"."qrtz_triggers" (
  "sched_name" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "job_name" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "job_group" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(250) COLLATE "pg_catalog"."default",
  "next_fire_time" int8,
  "prev_fire_time" int8,
  "priority" int4,
  "trigger_state" varchar(16) COLLATE "pg_catalog"."default" NOT NULL,
  "trigger_type" varchar(8) COLLATE "pg_catalog"."default" NOT NULL,
  "start_time" int8 NOT NULL,
  "end_time" int8,
  "calendar_name" varchar(200) COLLATE "pg_catalog"."default",
  "misfire_instr" int2,
  "job_data" bytea
)
;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO "public"."qrtz_triggers" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1623138820000, 1623138800000, 5, 'PAUSED', 'CRON', 1623138434000, 0, NULL, 2, E'\\\\x');
INSERT INTO "public"."qrtz_triggers" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1623138440000, -1, 5, 'PAUSED', 'CRON', 1623138434000, 0, NULL, 2, E'\\\\x');
INSERT INTO "public"."qrtz_triggers" VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1623138435000, -1, 5, 'PAUSED', 'CRON', 1623138433000, 0, NULL, 2, E'\\\\x');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_config";
CREATE TABLE "public"."sys_config" (
  "config_id" int4 NOT NULL DEFAULT nextval('config_id_seq'::regclass),
  "config_name" varchar(100) COLLATE "pg_catalog"."default",
  "config_key" varchar(100) COLLATE "pg_catalog"."default",
  "config_value" varchar(500) COLLATE "pg_catalog"."default",
  "config_type" char(1) COLLATE "pg_catalog"."default" DEFAULT 'N'::bpchar,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_config"."config_id" IS '参数主键';
COMMENT ON COLUMN "public"."sys_config"."config_name" IS '参数名称';
COMMENT ON COLUMN "public"."sys_config"."config_key" IS '参数键名';
COMMENT ON COLUMN "public"."sys_config"."config_value" IS '参数键值';
COMMENT ON COLUMN "public"."sys_config"."config_type" IS '系统内置（Y是 N否）';
COMMENT ON COLUMN "public"."sys_config"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_config"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_config"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_config"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_config"."remark" IS '备注';
COMMENT ON TABLE "public"."sys_config" IS '参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO "public"."sys_config" VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-06-04 13:57:09', '', NULL, '初始化密码 123456');
INSERT INTO "public"."sys_config" VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-06-04 13:57:09', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO "public"."sys_config" VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:50.684227', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_dept";
CREATE TABLE "public"."sys_dept" (
  "dept_id" int8 NOT NULL DEFAULT nextval('dept_id_seq'::regclass),
  "parent_id" int8 DEFAULT 0,
  "ancestors" varchar(50) COLLATE "pg_catalog"."default",
  "dept_name" varchar(30) COLLATE "pg_catalog"."default",
  "order_num" int4 DEFAULT 0,
  "leader" varchar(20) COLLATE "pg_catalog"."default",
  "phone" varchar(11) COLLATE "pg_catalog"."default",
  "email" varchar(50) COLLATE "pg_catalog"."default",
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "del_flag" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."sys_dept"."dept_id" IS '部门id';
COMMENT ON COLUMN "public"."sys_dept"."parent_id" IS '父部门id';
COMMENT ON COLUMN "public"."sys_dept"."ancestors" IS '祖级列表';
COMMENT ON COLUMN "public"."sys_dept"."dept_name" IS '部门名称';
COMMENT ON COLUMN "public"."sys_dept"."order_num" IS '显示顺序';
COMMENT ON COLUMN "public"."sys_dept"."leader" IS '负责人';
COMMENT ON COLUMN "public"."sys_dept"."phone" IS '联系电话';
COMMENT ON COLUMN "public"."sys_dept"."email" IS '邮箱';
COMMENT ON COLUMN "public"."sys_dept"."status" IS '部门状态（0正常 1停用）';
COMMENT ON COLUMN "public"."sys_dept"."del_flag" IS '删除标志（0代表存在 2代表删除）';
COMMENT ON COLUMN "public"."sys_dept"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_dept"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_dept"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_dept"."update_time" IS '更新时间';
COMMENT ON TABLE "public"."sys_dept" IS '部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO "public"."sys_dept" VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', '', NULL);
INSERT INTO "public"."sys_dept" VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', '', NULL);
INSERT INTO "public"."sys_dept" VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', '', NULL);
INSERT INTO "public"."sys_dept" VALUES (111, 110, '0,100,110', 'dgsf', 120, NULL, NULL, NULL, '0', '2', 'admin', '2021-06-07 20:56:01.588427', 'admin', '2021-06-07 20:56:09.469421');
INSERT INTO "public"."sys_dept" VALUES (110, 100, '0,100', 'TEST部门', 100, 'zc', '18988888888', '18988888888@qq.com', '0', '2', 'admin', '2021-06-07 20:55:36.725458', 'admin', '2021-06-07 20:55:54.049416');
INSERT INTO "public"."sys_dept" VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-07 20:57:41.010499');
INSERT INTO "public"."sys_dept" VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:41.162746');
INSERT INTO "public"."sys_dept" VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:41.166965');
INSERT INTO "public"."sys_dept" VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:41.166965');
INSERT INTO "public"."sys_dept" VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:41.166965');
INSERT INTO "public"."sys_dept" VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:41.166965');
INSERT INTO "public"."sys_dept" VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:41.166965');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_dict_data";
CREATE TABLE "public"."sys_dict_data" (
  "dict_code" int8 NOT NULL DEFAULT nextval('dict_code_seq'::regclass),
  "dict_sort" int4 DEFAULT 0,
  "dict_label" varchar(100) COLLATE "pg_catalog"."default",
  "dict_value" varchar(100) COLLATE "pg_catalog"."default",
  "dict_type" varchar(100) COLLATE "pg_catalog"."default",
  "css_class" varchar(100) COLLATE "pg_catalog"."default",
  "list_class" varchar(100) COLLATE "pg_catalog"."default",
  "is_default" char(1) COLLATE "pg_catalog"."default" DEFAULT 'N'::bpchar,
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_dict_data"."dict_code" IS '字典编码';
COMMENT ON COLUMN "public"."sys_dict_data"."dict_sort" IS '字典排序';
COMMENT ON COLUMN "public"."sys_dict_data"."dict_label" IS '字典标签';
COMMENT ON COLUMN "public"."sys_dict_data"."dict_value" IS '字典键值';
COMMENT ON COLUMN "public"."sys_dict_data"."dict_type" IS '字典类型';
COMMENT ON COLUMN "public"."sys_dict_data"."css_class" IS '样式属性（其他样式扩展）';
COMMENT ON COLUMN "public"."sys_dict_data"."list_class" IS '表格回显样式';
COMMENT ON COLUMN "public"."sys_dict_data"."is_default" IS '是否默认（Y是 N否）';
COMMENT ON COLUMN "public"."sys_dict_data"."status" IS '状态（0正常 1停用）';
COMMENT ON COLUMN "public"."sys_dict_data"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_dict_data"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_dict_data"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_dict_data"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_dict_data"."remark" IS '备注';
COMMENT ON TABLE "public"."sys_dict_data" IS '字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO "public"."sys_dict_data" VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '显示菜单');
INSERT INTO "public"."sys_dict_data" VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '隐藏菜单');
INSERT INTO "public"."sys_dict_data" VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '正常状态');
INSERT INTO "public"."sys_dict_data" VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '停用状态');
INSERT INTO "public"."sys_dict_data" VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '正常状态');
INSERT INTO "public"."sys_dict_data" VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '停用状态');
INSERT INTO "public"."sys_dict_data" VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '默认分组');
INSERT INTO "public"."sys_dict_data" VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '系统分组');
INSERT INTO "public"."sys_dict_data" VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '系统默认是');
INSERT INTO "public"."sys_dict_data" VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '系统默认否');
INSERT INTO "public"."sys_dict_data" VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '通知');
INSERT INTO "public"."sys_dict_data" VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '公告');
INSERT INTO "public"."sys_dict_data" VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '正常状态');
INSERT INTO "public"."sys_dict_data" VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '关闭状态');
INSERT INTO "public"."sys_dict_data" VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '新增操作');
INSERT INTO "public"."sys_dict_data" VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '修改操作');
INSERT INTO "public"."sys_dict_data" VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '删除操作');
INSERT INTO "public"."sys_dict_data" VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '授权操作');
INSERT INTO "public"."sys_dict_data" VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '导出操作');
INSERT INTO "public"."sys_dict_data" VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '导入操作');
INSERT INTO "public"."sys_dict_data" VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '强退操作');
INSERT INTO "public"."sys_dict_data" VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '生成操作');
INSERT INTO "public"."sys_dict_data" VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '清空操作');
INSERT INTO "public"."sys_dict_data" VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '正常状态');
INSERT INTO "public"."sys_dict_data" VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '停用状态');
INSERT INTO "public"."sys_dict_data" VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '性别男');
INSERT INTO "public"."sys_dict_data" VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '性别女');
INSERT INTO "public"."sys_dict_data" VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '性别未知');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_dict_type";
CREATE TABLE "public"."sys_dict_type" (
  "dict_id" int8 NOT NULL DEFAULT nextval('dict_id_seq'::regclass),
  "dict_name" varchar(100) COLLATE "pg_catalog"."default",
  "dict_type" varchar(100) COLLATE "pg_catalog"."default",
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_dict_type"."dict_id" IS '字典主键';
COMMENT ON COLUMN "public"."sys_dict_type"."dict_name" IS '字典名称';
COMMENT ON COLUMN "public"."sys_dict_type"."dict_type" IS '字典类型';
COMMENT ON COLUMN "public"."sys_dict_type"."status" IS '状态（0正常 1停用）';
COMMENT ON COLUMN "public"."sys_dict_type"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_dict_type"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_dict_type"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_dict_type"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_dict_type"."remark" IS '备注';
COMMENT ON TABLE "public"."sys_dict_type" IS '字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO "public"."sys_dict_type" VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '菜单状态列表');
INSERT INTO "public"."sys_dict_type" VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '系统开关列表');
INSERT INTO "public"."sys_dict_type" VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '任务状态列表');
INSERT INTO "public"."sys_dict_type" VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '任务分组列表');
INSERT INTO "public"."sys_dict_type" VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '系统是否列表');
INSERT INTO "public"."sys_dict_type" VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '通知类型列表');
INSERT INTO "public"."sys_dict_type" VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '通知状态列表');
INSERT INTO "public"."sys_dict_type" VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '操作类型列表');
INSERT INTO "public"."sys_dict_type" VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '登录状态列表');
INSERT INTO "public"."sys_dict_type" VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:47.445089', '用户性别列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_job";
CREATE TABLE "public"."sys_job" (
  "job_id" int8 NOT NULL DEFAULT nextval('job_id_seq'::regclass),
  "job_name" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "job_group" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT 'DEFAULT'::character varying,
  "invoke_target" varchar(500) COLLATE "pg_catalog"."default" NOT NULL,
  "cron_expression" varchar(255) COLLATE "pg_catalog"."default",
  "misfire_policy" varchar(20) COLLATE "pg_catalog"."default" DEFAULT '3'::character varying,
  "concurrent" char(1) COLLATE "pg_catalog"."default" DEFAULT '1'::bpchar,
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_job"."job_id" IS '任务ID';
COMMENT ON COLUMN "public"."sys_job"."job_name" IS '任务名称';
COMMENT ON COLUMN "public"."sys_job"."job_group" IS '任务组名';
COMMENT ON COLUMN "public"."sys_job"."invoke_target" IS '调用目标字符串';
COMMENT ON COLUMN "public"."sys_job"."cron_expression" IS 'cron执行表达式';
COMMENT ON COLUMN "public"."sys_job"."misfire_policy" IS '计划执行错误策略（1立即执行 2执行一次 3放弃执行）';
COMMENT ON COLUMN "public"."sys_job"."concurrent" IS '是否并发执行（0允许 1禁止）';
COMMENT ON COLUMN "public"."sys_job"."status" IS '状态（0正常 1暂停）';
COMMENT ON COLUMN "public"."sys_job"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_job"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_job"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_job"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_job"."remark" IS '备注信息';
COMMENT ON TABLE "public"."sys_job" IS '定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO "public"."sys_job" VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(''ry'')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-06-04 13:57:10', '', NULL, '');
INSERT INTO "public"."sys_job" VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-06-04 13:57:10', '', '2021-06-08 11:25:25.866246', '');
INSERT INTO "public"."sys_job" VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-06-04 13:57:10', '', '2021-06-08 15:53:33.181697', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_job_log";
CREATE TABLE "public"."sys_job_log" (
  "job_log_id" int8 NOT NULL DEFAULT nextval('job_log_id_seq'::regclass),
  "job_name" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "job_group" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "invoke_target" varchar(500) COLLATE "pg_catalog"."default" NOT NULL,
  "job_message" varchar(500) COLLATE "pg_catalog"."default",
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "exception_info" varchar(2000) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."sys_job_log"."job_log_id" IS '任务日志ID';
COMMENT ON COLUMN "public"."sys_job_log"."job_name" IS '任务名称';
COMMENT ON COLUMN "public"."sys_job_log"."job_group" IS '任务组名';
COMMENT ON COLUMN "public"."sys_job_log"."invoke_target" IS '调用目标字符串';
COMMENT ON COLUMN "public"."sys_job_log"."job_message" IS '日志信息';
COMMENT ON COLUMN "public"."sys_job_log"."status" IS '执行状态（0正常 1失败）';
COMMENT ON COLUMN "public"."sys_job_log"."exception_info" IS '异常信息';
COMMENT ON COLUMN "public"."sys_job_log"."create_time" IS '创建时间';
COMMENT ON TABLE "public"."sys_job_log" IS '定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO "public"."sys_job_log" VALUES (2, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', NULL, '2021-06-08 11:27:51.706581');
INSERT INTO "public"."sys_job_log" VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：2毫秒', '0', NULL, '2021-06-08 11:27:58.950835');
INSERT INTO "public"."sys_job_log" VALUES (4, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(''ry'')', '系统默认（有参） 总共耗时：0毫秒', '0', NULL, '2021-06-08 11:28:03.317771');
INSERT INTO "public"."sys_job_log" VALUES (5, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：1毫秒', '0', NULL, '2021-06-08 15:51:32.39725');
INSERT INTO "public"."sys_job_log" VALUES (6, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：3毫秒', '0', NULL, '2021-06-08 15:51:48.14402');
INSERT INTO "public"."sys_job_log" VALUES (7, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(''ry'')', '系统默认（有参） 总共耗时：1毫秒', '0', NULL, '2021-06-08 15:51:54.341716');
INSERT INTO "public"."sys_job_log" VALUES (8, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', NULL, '2021-06-08 15:52:27.22136');
INSERT INTO "public"."sys_job_log" VALUES (9, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', NULL, '2021-06-08 15:52:47.219314');
INSERT INTO "public"."sys_job_log" VALUES (10, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', NULL, '2021-06-08 15:53:07.216819');
INSERT INTO "public"."sys_job_log" VALUES (11, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', NULL, '2021-06-08 15:53:27.21949');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_logininfor";
CREATE TABLE "public"."sys_logininfor" (
  "info_id" int8 NOT NULL DEFAULT nextval('info_id_seq'::regclass),
  "user_name" varchar(50) COLLATE "pg_catalog"."default",
  "ipaddr" varchar(128) COLLATE "pg_catalog"."default",
  "login_location" varchar(255) COLLATE "pg_catalog"."default",
  "browser" varchar(50) COLLATE "pg_catalog"."default",
  "os" varchar(50) COLLATE "pg_catalog"."default",
  "status" char(1) COLLATE "pg_catalog"."default",
  "msg" varchar(255) COLLATE "pg_catalog"."default",
  "login_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."sys_logininfor"."info_id" IS '访问ID';
COMMENT ON COLUMN "public"."sys_logininfor"."user_name" IS '用户账号';
COMMENT ON COLUMN "public"."sys_logininfor"."ipaddr" IS '登录IP地址';
COMMENT ON COLUMN "public"."sys_logininfor"."login_location" IS '登录地点';
COMMENT ON COLUMN "public"."sys_logininfor"."browser" IS '浏览器类型';
COMMENT ON COLUMN "public"."sys_logininfor"."os" IS '操作系统';
COMMENT ON COLUMN "public"."sys_logininfor"."status" IS '登录状态（0成功 1失败）';
COMMENT ON COLUMN "public"."sys_logininfor"."msg" IS '提示消息';
COMMENT ON COLUMN "public"."sys_logininfor"."login_time" IS '访问时间';
COMMENT ON TABLE "public"."sys_logininfor" IS '系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO "public"."sys_logininfor" VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-04 16:22:32');
INSERT INTO "public"."sys_logininfor" VALUES (2, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-04 16:22:55');
INSERT INTO "public"."sys_logininfor" VALUES (3, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-04 16:23:10');
INSERT INTO "public"."sys_logininfor" VALUES (4, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 10:06:20.702287');
INSERT INTO "public"."sys_logininfor" VALUES (5, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 10:25:54.379521');
INSERT INTO "public"."sys_logininfor" VALUES (6, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 10:36:54.71922');
INSERT INTO "public"."sys_logininfor" VALUES (7, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 10:52:38.828959');
INSERT INTO "public"."sys_logininfor" VALUES (8, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 11:12:42.18683');
INSERT INTO "public"."sys_logininfor" VALUES (9, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 11:17:02.381307');
INSERT INTO "public"."sys_logininfor" VALUES (10, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 11:19:54.344448');
INSERT INTO "public"."sys_logininfor" VALUES (11, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 11:19:57.523695');
INSERT INTO "public"."sys_logininfor" VALUES (12, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 11:22:58.388492');
INSERT INTO "public"."sys_logininfor" VALUES (13, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 13:31:59.043807');
INSERT INTO "public"."sys_logininfor" VALUES (14, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 13:32:04.07422');
INSERT INTO "public"."sys_logininfor" VALUES (15, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 13:32:07.103494');
INSERT INTO "public"."sys_logininfor" VALUES (16, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 13:32:09.669301');
INSERT INTO "public"."sys_logininfor" VALUES (17, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 13:33:18.382693');
INSERT INTO "public"."sys_logininfor" VALUES (18, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 13:35:57.522909');
INSERT INTO "public"."sys_logininfor" VALUES (19, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 14:15:05.879202');
INSERT INTO "public"."sys_logininfor" VALUES (20, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:15:08.524295');
INSERT INTO "public"."sys_logininfor" VALUES (21, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 14:18:10.117016');
INSERT INTO "public"."sys_logininfor" VALUES (22, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 14:18:14.440012');
INSERT INTO "public"."sys_logininfor" VALUES (23, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:18:17.10989');
INSERT INTO "public"."sys_logininfor" VALUES (24, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:19:51.367647');
INSERT INTO "public"."sys_logininfor" VALUES (25, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:22:56.821989');
INSERT INTO "public"."sys_logininfor" VALUES (26, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:47:19.822246');
INSERT INTO "public"."sys_logininfor" VALUES (27, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 15:29:34.855464');
INSERT INTO "public"."sys_logininfor" VALUES (28, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 15:46:36.427681');
INSERT INTO "public"."sys_logininfor" VALUES (29, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 15:46:51.54465');
INSERT INTO "public"."sys_logininfor" VALUES (30, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 15:48:12.138992');
INSERT INTO "public"."sys_logininfor" VALUES (31, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 15:48:16.524028');
INSERT INTO "public"."sys_logininfor" VALUES (32, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 15:48:19.724476');
INSERT INTO "public"."sys_logininfor" VALUES (33, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 15:49:26.871194');
INSERT INTO "public"."sys_logininfor" VALUES (34, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-06-07 15:49:33.282371');
INSERT INTO "public"."sys_logininfor" VALUES (35, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 15:49:43.471557');
INSERT INTO "public"."sys_logininfor" VALUES (36, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 15:50:54.470068');
INSERT INTO "public"."sys_logininfor" VALUES (37, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 15:51:00.527662');
INSERT INTO "public"."sys_logininfor" VALUES (38, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 16:04:32.34047');
INSERT INTO "public"."sys_logininfor" VALUES (39, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 16:22:20.17496');
INSERT INTO "public"."sys_logininfor" VALUES (40, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 16:38:05.315805');
INSERT INTO "public"."sys_logininfor" VALUES (41, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 16:39:28.697443');
INSERT INTO "public"."sys_logininfor" VALUES (42, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 16:39:36.137584');
INSERT INTO "public"."sys_logininfor" VALUES (43, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 16:39:44.896759');
INSERT INTO "public"."sys_logininfor" VALUES (44, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 16:39:48.423656');
INSERT INTO "public"."sys_logininfor" VALUES (45, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:09:55.820718');
INSERT INTO "public"."sys_logininfor" VALUES (46, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 17:14:22.456066');
INSERT INTO "public"."sys_logininfor" VALUES (47, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:14:25.242127');
INSERT INTO "public"."sys_logininfor" VALUES (48, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:33:47.449919');
INSERT INTO "public"."sys_logininfor" VALUES (49, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 17:35:11.891013');
INSERT INTO "public"."sys_logininfor" VALUES (50, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 17:35:22.155845');
INSERT INTO "public"."sys_logininfor" VALUES (51, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:35:26.699779');
INSERT INTO "public"."sys_logininfor" VALUES (52, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:37:19.893497');
INSERT INTO "public"."sys_logininfor" VALUES (53, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 17:47:51.324949');
INSERT INTO "public"."sys_logininfor" VALUES (54, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-06-07 17:47:58.026741');
INSERT INTO "public"."sys_logininfor" VALUES (55, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-06-07 17:48:02.4888');
INSERT INTO "public"."sys_logininfor" VALUES (56, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:48:08.675299');
INSERT INTO "public"."sys_logininfor" VALUES (57, 'test', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 17:48:45.657022');
INSERT INTO "public"."sys_logininfor" VALUES (58, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 17:48:48.718861');
INSERT INTO "public"."sys_logininfor" VALUES (59, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 17:48:53.342561');
INSERT INTO "public"."sys_logininfor" VALUES (60, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:48:58.675617');
INSERT INTO "public"."sys_logininfor" VALUES (61, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:51:04.258246');
INSERT INTO "public"."sys_logininfor" VALUES (62, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-07 20:37:54.764405');
INSERT INTO "public"."sys_logininfor" VALUES (63, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-07 22:11:45.506834');
INSERT INTO "public"."sys_logininfor" VALUES (64, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-07 22:15:54.44133');
INSERT INTO "public"."sys_logininfor" VALUES (65, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:25:48.502869');
INSERT INTO "public"."sys_logininfor" VALUES (67, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码已失效', '2021-06-08 11:25:06.845963');
INSERT INTO "public"."sys_logininfor" VALUES (68, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 11:25:10.994552');
INSERT INTO "public"."sys_logininfor" VALUES (69, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 13:33:06.471091');
INSERT INTO "public"."sys_logininfor" VALUES (70, 'admin', '192.168.28.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 15:47:39.229223');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_menu";
CREATE TABLE "public"."sys_menu" (
  "menu_id" int8 NOT NULL DEFAULT nextval('menu_id_seq'::regclass),
  "menu_name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "parent_id" int8 DEFAULT 0,
  "order_num" int4 DEFAULT 0,
  "path" varchar(200) COLLATE "pg_catalog"."default",
  "component" varchar(255) COLLATE "pg_catalog"."default",
  "is_frame" int4 DEFAULT 1,
  "is_cache" int4 DEFAULT 0,
  "menu_type" char(1) COLLATE "pg_catalog"."default",
  "visible" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "perms" varchar(100) COLLATE "pg_catalog"."default",
  "icon" varchar(100) COLLATE "pg_catalog"."default" DEFAULT '#'::character varying,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_menu"."menu_id" IS '菜单ID';
COMMENT ON COLUMN "public"."sys_menu"."menu_name" IS '菜单名称';
COMMENT ON COLUMN "public"."sys_menu"."parent_id" IS '父菜单ID';
COMMENT ON COLUMN "public"."sys_menu"."order_num" IS '显示顺序';
COMMENT ON COLUMN "public"."sys_menu"."path" IS '路由地址';
COMMENT ON COLUMN "public"."sys_menu"."component" IS '组件路径';
COMMENT ON COLUMN "public"."sys_menu"."is_frame" IS '是否为外链（0是 1否）';
COMMENT ON COLUMN "public"."sys_menu"."is_cache" IS '是否缓存（0缓存 1不缓存）';
COMMENT ON COLUMN "public"."sys_menu"."menu_type" IS '菜单类型（M目录 C菜单 F按钮）';
COMMENT ON COLUMN "public"."sys_menu"."visible" IS '菜单状态（0显示 1隐藏）';
COMMENT ON COLUMN "public"."sys_menu"."status" IS '菜单状态（0正常 1停用）';
COMMENT ON COLUMN "public"."sys_menu"."perms" IS '权限标识';
COMMENT ON COLUMN "public"."sys_menu"."icon" IS '菜单图标';
COMMENT ON COLUMN "public"."sys_menu"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_menu"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_menu"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_menu"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_menu"."remark" IS '备注';
COMMENT ON TABLE "public"."sys_menu" IS '菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO "public"."sys_menu" VALUES (1, '系统管理', 0, 1, 'system', NULL, 1, 0, 'M', '0', '0', '', 'system', 'admin', '2021-06-04 13:57:09', '', NULL, '系统管理目录');
INSERT INTO "public"."sys_menu" VALUES (3, '系统工具', 0, 3, 'tool', NULL, 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2021-06-04 13:57:09', '', NULL, '系统工具目录');
INSERT INTO "public"."sys_menu" VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, 0, 0, 'M', '0', '0', '', 'guide', 'admin', '2021-06-04 13:57:09', '', NULL, '若依官网地址');
INSERT INTO "public"."sys_menu" VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2021-06-04 13:57:09', '', NULL, '用户管理菜单');
INSERT INTO "public"."sys_menu" VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2021-06-04 13:57:09', '', NULL, '角色管理菜单');
INSERT INTO "public"."sys_menu" VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2021-06-04 13:57:09', '', NULL, '菜单管理菜单');
INSERT INTO "public"."sys_menu" VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2021-06-04 13:57:09', '', NULL, '部门管理菜单');
INSERT INTO "public"."sys_menu" VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2021-06-04 13:57:09', '', NULL, '岗位管理菜单');
INSERT INTO "public"."sys_menu" VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2021-06-04 13:57:09', '', NULL, '字典管理菜单');
INSERT INTO "public"."sys_menu" VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2021-06-04 13:57:09', '', NULL, '参数设置菜单');
INSERT INTO "public"."sys_menu" VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2021-06-04 13:57:09', '', NULL, '通知公告菜单');
INSERT INTO "public"."sys_menu" VALUES (108, '日志管理', 1, 9, 'log', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2021-06-04 13:57:09', '', NULL, '日志管理菜单');
INSERT INTO "public"."sys_menu" VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2021-06-04 13:57:09', '', NULL, '在线用户菜单');
INSERT INTO "public"."sys_menu" VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2021-06-04 13:57:09', '', NULL, '定时任务菜单');
INSERT INTO "public"."sys_menu" VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2021-06-04 13:57:09', '', NULL, '数据监控菜单');
INSERT INTO "public"."sys_menu" VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2021-06-04 13:57:09', '', NULL, '服务监控菜单');
INSERT INTO "public"."sys_menu" VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2021-06-04 13:57:09', '', NULL, '缓存监控菜单');
INSERT INTO "public"."sys_menu" VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2021-06-04 13:57:09', '', NULL, '表单构建菜单');
INSERT INTO "public"."sys_menu" VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2021-06-04 13:57:09', '', NULL, '代码生成菜单');
INSERT INTO "public"."sys_menu" VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2021-06-04 13:57:09', '', NULL, '系统接口菜单');
INSERT INTO "public"."sys_menu" VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2021-06-04 13:57:09', '', NULL, '操作日志菜单');
INSERT INTO "public"."sys_menu" VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2021-06-04 13:57:09', '', NULL, '登录日志菜单');
INSERT INTO "public"."sys_menu" VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1019, '部门修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1020, '部门删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1036, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1037, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1038, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1039, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1040, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1041, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1042, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1043, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1044, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1045, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1046, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1047, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1048, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1049, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1050, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1051, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1052, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1053, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1054, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1055, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1056, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1057, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1058, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1059, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (1060, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_menu" VALUES (2, '系统监控', 0, 2, 'monitor', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:37.84362', '系统监控目录');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_notice";
CREATE TABLE "public"."sys_notice" (
  "notice_id" int4 NOT NULL DEFAULT nextval('notice_id_seq'::regclass),
  "notice_title" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "notice_type" char(1) COLLATE "pg_catalog"."default" NOT NULL,
  "notice_content" bytea,
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_notice"."notice_id" IS '公告ID';
COMMENT ON COLUMN "public"."sys_notice"."notice_title" IS '公告标题';
COMMENT ON COLUMN "public"."sys_notice"."notice_type" IS '公告类型（1通知 2公告）';
COMMENT ON COLUMN "public"."sys_notice"."notice_content" IS '公告内容';
COMMENT ON COLUMN "public"."sys_notice"."status" IS '公告状态（0正常 1关闭）';
COMMENT ON COLUMN "public"."sys_notice"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_notice"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_notice"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_notice"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_notice"."remark" IS '备注';
COMMENT ON TABLE "public"."sys_notice" IS '通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO "public"."sys_notice" VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', E'\\346\\226\\260\\347\\211\\210\\346\\234\\254\\345\\206\\205\\345\\256\\271', '0', 'admin', '2021-06-04 13:57:10', '', NULL, '管理员');
INSERT INTO "public"."sys_notice" VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', E'<p>\\347\\273\\264\\346\\212\\244\\345\\206\\205\\345\\256\\271</p>', '0', 'admin', '2021-06-04 13:57:10', 'admin', '2021-06-08 15:51:04.868894', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_oper_log";
CREATE TABLE "public"."sys_oper_log" (
  "oper_id" int8 NOT NULL DEFAULT nextval('oper_id_seq'::regclass),
  "title" varchar(50) COLLATE "pg_catalog"."default",
  "business_type" int4,
  "method" varchar(100) COLLATE "pg_catalog"."default",
  "request_method" varchar(10) COLLATE "pg_catalog"."default",
  "operator_type" int4,
  "oper_name" varchar(50) COLLATE "pg_catalog"."default",
  "dept_name" varchar(50) COLLATE "pg_catalog"."default",
  "oper_url" varchar(255) COLLATE "pg_catalog"."default",
  "oper_ip" varchar(128) COLLATE "pg_catalog"."default",
  "oper_location" varchar(255) COLLATE "pg_catalog"."default",
  "oper_param" varchar(2000) COLLATE "pg_catalog"."default",
  "json_result" varchar(2000) COLLATE "pg_catalog"."default",
  "status" int4,
  "error_msg" varchar(2000) COLLATE "pg_catalog"."default",
  "oper_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."sys_oper_log"."oper_id" IS '日志主键';
COMMENT ON COLUMN "public"."sys_oper_log"."title" IS '模块标题';
COMMENT ON COLUMN "public"."sys_oper_log"."business_type" IS '业务类型（0其它 1新增 2修改 3删除）';
COMMENT ON COLUMN "public"."sys_oper_log"."method" IS '方法名称';
COMMENT ON COLUMN "public"."sys_oper_log"."request_method" IS '请求方式';
COMMENT ON COLUMN "public"."sys_oper_log"."operator_type" IS '操作类别（0其它 1后台用户 2手机端用户）';
COMMENT ON COLUMN "public"."sys_oper_log"."oper_name" IS '操作人员';
COMMENT ON COLUMN "public"."sys_oper_log"."dept_name" IS '部门名称';
COMMENT ON COLUMN "public"."sys_oper_log"."oper_url" IS '请求URL';
COMMENT ON COLUMN "public"."sys_oper_log"."oper_ip" IS '主机地址';
COMMENT ON COLUMN "public"."sys_oper_log"."oper_location" IS '操作地点';
COMMENT ON COLUMN "public"."sys_oper_log"."oper_param" IS '请求参数';
COMMENT ON COLUMN "public"."sys_oper_log"."json_result" IS '返回参数';
COMMENT ON COLUMN "public"."sys_oper_log"."status" IS '操作状态（0正常 1异常）';
COMMENT ON COLUMN "public"."sys_oper_log"."error_msg" IS '错误消息';
COMMENT ON COLUMN "public"."sys_oper_log"."oper_time" IS '操作时间';
COMMENT ON TABLE "public"."sys_oper_log" IS '操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO "public"."sys_oper_log" VALUES (1, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"phonenumber":"18988888888","admin":false,"password":"$2a$10$Dn3/dGRok7cBHg6RnFyqD.YmeWoMyUY6bbCD0tbJ1jQv6jxna/H92","postIds":[1],"email":"18988888888@qq.com","nickName":"test","sex":"0","deptId":108,"params":{},"userName":"test","userId":3,"createBy":"admin","roleIds":[2],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 15:41:00.737524');
INSERT INTO "public"."sys_oper_log" VALUES (2, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"phonenumber":"18988888888","admin":false,"password":"$2a$10$ReuU5NiL6RLE7/gNdZjN9OxgvBJ/8A86AOcwm9xpWzCEp2Spt6w4O","postIds":[],"email":"18988888888@qq.com","nickName":"test","deptId":109,"params":{},"userName":"test","userId":4,"createBy":"admin","roleIds":[],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 15:45:54.340099');
INSERT INTO "public"."sys_oper_log" VALUES (3, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"roles":[],"phonenumber":"18988888888","admin":false,"loginDate":1623052011533,"delFlag":"0","password":"","updateBy":"admin","postIds":[],"loginIp":"127.0.0.1","email":"18988888888@qq.com","nickName":"测试","sex":"0","deptId":109,"dept":{"deptName":"财务部门","leader":"若依","deptId":109,"orderNum":"2","params":{},"parentId":102,"children":[],"status":"0"},"params":{},"userName":"test","userId":4,"createBy":"admin","roleIds":[],"createTime":1623051954000,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 15:49:02.91995');
INSERT INTO "public"."sys_oper_log" VALUES (4, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.resetPwd()', 'PUT', 1, 'admin', NULL, '/system/user/resetPwd', '127.0.0.1', '内网IP', '{"admin":false,"password":"$2a$10$pjVeJvsc1BtGxbFBqRqZIO8pCbMW2lmxgKPGH50a3EHa4.re4MMXG","updateBy":"admin","params":{},"userId":4}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 15:49:22.140025');
INSERT INTO "public"."sys_oper_log" VALUES (5, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, 'test', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{"msg":"操作成功","imgUrl":"/profile/avatar/2021/06/07/a6a509ed-5819-4176-983f-b1f00aeca6f6.jpeg","code":200}', 0, NULL, '2021-06-07 15:50:29.993384');
INSERT INTO "public"."sys_oper_log" VALUES (6, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"1"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "menu_check_strictly" 的类型为 smallint, 但表达式的类型为 boolean
  建议：你需要重写或转换表达式
  位置：47
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysRoleMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysRoleMapper.updateRole-Inline
### The error occurred while setting parameters
### SQL: update sys_role     SET menu_check_strictly = ?,     dept_check_strictly = ?,     status = ?,          update_by = ?,     update_time = now()     where role_id = ?
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "menu_check_strictly" 的类型为 smallint, 但表达式的类型为 boolean
  建议：你需要重写或转换表达式
  位置：47
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "menu_check_strictly" 的类型为 smallint, 但表达式的类型为 boolean
  建议：你需要重写或转换表达式
  位置：47', '2021-06-07 16:05:05.594099');
INSERT INTO "public"."sys_oper_log" VALUES (7, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:16:47.343078');
INSERT INTO "public"."sys_oper_log" VALUES (8, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:16:49.089064');
INSERT INTO "public"."sys_oper_log" VALUES (9, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":1,"admin":true,"params":{},"deptCheckStrictly":false,"menuCheckStrictly":false,"status":"1"}', 'null', 1, '不允许操作超级管理员角色', '2021-06-07 16:16:50.59593');
INSERT INTO "public"."sys_oper_log" VALUES (10, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:16:52.795186');
INSERT INTO "public"."sys_oper_log" VALUES (11, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:17:20.081464');
INSERT INTO "public"."sys_oper_log" VALUES (12, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:18:12.894178');
INSERT INTO "public"."sys_oper_log" VALUES (13, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:18:21.917427');
INSERT INTO "public"."sys_oper_log" VALUES (14, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:18:32.055327');
INSERT INTO "public"."sys_oper_log" VALUES (15, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:18:40.227994');
INSERT INTO "public"."sys_oper_log" VALUES (16, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:18:59.884785');
INSERT INTO "public"."sys_oper_log" VALUES (58, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:38:15.00527');
INSERT INTO "public"."sys_oper_log" VALUES (17, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":true,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116,4],"status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：76
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysRoleMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysRoleMapper.updateRole-Inline
### The error occurred while setting parameters
### SQL: update sys_role     SET role_name = ?,     role_key = ?,     role_sort = ?,     data_scope = ?,     menu_check_strictly = ?,     dept_check_strictly = ?,     status = ?,     remark = ?,     update_by = ?,     update_time = now()     where role_id = ?
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：76
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：76', '2021-06-07 16:19:32.03359');
INSERT INTO "public"."sys_oper_log" VALUES (18, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":true,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116,4],"status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：76
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysRoleMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysRoleMapper.updateRole-Inline
### The error occurred while setting parameters
### SQL: update sys_role     SET role_name = ?,     role_key = ?,     role_sort = ?,     data_scope = ?,     menu_check_strictly = ?,     dept_check_strictly = ?,     status = ?,     remark = ?,     update_by = ?,     update_time = now()     where role_id = ?
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：76
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：76', '2021-06-07 16:21:50.932701');
INSERT INTO "public"."sys_oper_log" VALUES (19, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{"admin":false,"updateBy":"admin","params":{},"userId":4,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:22:28.986806');
INSERT INTO "public"."sys_oper_log" VALUES (20, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{"admin":false,"updateBy":"admin","params":{},"userId":4,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:22:41.077812');
INSERT INTO "public"."sys_oper_log" VALUES (21, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{"admin":false,"updateBy":"admin","params":{},"userId":4,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:22:52.166919');
INSERT INTO "public"."sys_oper_log" VALUES (22, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{"admin":false,"updateBy":"admin","params":{},"userId":4,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:23:14.536266');
INSERT INTO "public"."sys_oper_log" VALUES (23, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:23:34.376536');
INSERT INTO "public"."sys_oper_log" VALUES (24, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:38:14.063036');
INSERT INTO "public"."sys_oper_log" VALUES (25, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:38:15.579509');
INSERT INTO "public"."sys_oper_log" VALUES (26, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":false,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116,4],"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:38:26.29112');
INSERT INTO "public"."sys_oper_log" VALUES (27, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":false,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116,4],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:38:37.397909');
INSERT INTO "public"."sys_oper_log" VALUES (28, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":false,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116,4],"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:38:43.792269');
INSERT INTO "public"."sys_oper_log" VALUES (29, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":false,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:38:57.015293');
INSERT INTO "public"."sys_oper_log" VALUES (30, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":false,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,114,115,1055,1056,1058,1057,1059,1060,116,4],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:39:04.146089');
INSERT INTO "public"."sys_oper_log" VALUES (31, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"roles":[],"phonenumber":"18988888888","admin":false,"loginDate":1623052183459,"delFlag":"0","password":"","updateBy":"admin","postIds":[],"loginIp":"127.0.0.1","email":"18988888888@qq.com","nickName":"测试","sex":"0","deptId":109,"avatar":"/profile/avatar/2021/06/07/a6a509ed-5819-4176-983f-b1f00aeca6f6.jpeg","dept":{"deptName":"财务部门","leader":"若依","deptId":109,"orderNum":"2","params":{},"parentId":102,"children":[],"status":"0"},"params":{},"userName":"test","userId":4,"createBy":"admin","roleIds":[2],"createTime":1623051954000,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:39:26.048869');
INSERT INTO "public"."sys_oper_log" VALUES (32, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":false,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116,4],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:39:58.539662');
INSERT INTO "public"."sys_oper_log" VALUES (33, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":false,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,1055,1056,1058,1057,1059,1060,4],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 16:40:10.225472');
INSERT INTO "public"."sys_oper_log" VALUES (34, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{"admin":true,"params":{},"userId":1,"status":"1"}', 'null', 1, '不允许操作超级管理员用户', '2021-06-07 17:37:34.848045');
INSERT INTO "public"."sys_oper_log" VALUES (35, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{"admin":false,"updateBy":"admin","params":{},"userId":2,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:37:36.719486');
INSERT INTO "public"."sys_oper_log" VALUES (36, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{"admin":false,"updateBy":"admin","params":{},"userId":2,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:37:38.040102');
INSERT INTO "public"."sys_oper_log" VALUES (37, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"roles":[{"flag":false,"roleId":2,"admin":false,"dataScope":"2","params":{},"roleSort":"2","deptCheckStrictly":false,"menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","status":"0"}],"phonenumber":"15666666666","admin":false,"loginDate":1622786229000,"remark":"测试员","delFlag":"0","password":"","updateBy":"admin","postIds":[2],"loginIp":"127.0.0.1","email":"ry@qq.com","nickName":"若依","sex":"1","deptId":105,"avatar":"","dept":{"deptName":"测试部门","leader":"若依","deptId":105,"orderNum":"3","params":{},"parentId":101,"children":[],"status":"0"},"params":{},"userName":"ry","userId":2,"createBy":"admin","roleIds":[2],"createTime":1622786229000,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:37:40.966939');
INSERT INTO "public"."sys_oper_log" VALUES (38, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"roles":[{"flag":false,"roleId":2,"admin":false,"dataScope":"2","params":{},"roleSort":"2","deptCheckStrictly":false,"menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","status":"0"}],"phonenumber":"15666666666","admin":false,"loginDate":1622786229000,"remark":"测试员","delFlag":"0","password":"","updateBy":"admin","postIds":[2],"loginIp":"127.0.0.1","email":"ry@qq.com","nickName":"若依","sex":"1","deptId":105,"avatar":"","dept":{"deptName":"测试部门","leader":"若依","deptId":105,"orderNum":"3","params":{},"parentId":101,"children":[],"status":"0"},"params":{},"userName":"ry","userId":2,"createBy":"admin","roleIds":[2],"createTime":1622786229000,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:37:45.531104');
INSERT INTO "public"."sys_oper_log" VALUES (39, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"roles":[{"flag":false,"roleId":2,"admin":false,"dataScope":"2","params":{},"roleSort":"2","deptCheckStrictly":false,"menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","status":"0"}],"phonenumber":"18988888888","admin":false,"loginDate":1623055176125,"delFlag":"0","password":"","updateBy":"admin","postIds":[],"loginIp":"127.0.0.1","email":"18988888888@qq.com","nickName":"测试11","sex":"0","deptId":109,"avatar":"/profile/avatar/2021/06/07/a6a509ed-5819-4176-983f-b1f00aeca6f6.jpeg","dept":{"deptName":"财务部门","leader":"若依","deptId":109,"orderNum":"2","params":{},"parentId":102,"children":[],"status":"0"},"params":{},"userName":"test","userId":4,"createBy":"admin","roleIds":[2],"createTime":1623051954000,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:37:52.480173');
INSERT INTO "public"."sys_oper_log" VALUES (40, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"roles":[{"flag":false,"roleId":2,"admin":false,"dataScope":"2","params":{},"roleSort":"2","deptCheckStrictly":false,"menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","status":"0"}],"phonenumber":"18988888888","admin":false,"loginDate":1623055176125,"delFlag":"0","password":"","updateBy":"admin","postIds":[],"loginIp":"127.0.0.1","email":"18988888888@qq.com","nickName":"测试","sex":"0","deptId":109,"avatar":"/profile/avatar/2021/06/07/a6a509ed-5819-4176-983f-b1f00aeca6f6.jpeg","dept":{"deptName":"财务部门","leader":"若依","deptId":109,"orderNum":"2","params":{},"parentId":102,"children":[],"status":"0"},"params":{},"userName":"test","userId":4,"createBy":"admin","roleIds":[2],"createTime":1623051954000,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:37:57.410971');
INSERT INTO "public"."sys_oper_log" VALUES (41, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"admin":false,"params":{},"roleSort":"0","deptCheckStrictly":true,"createBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试","deptIds":[],"menuIds":[2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：235
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysRoleMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysRoleMapper.insertRole-Inline
### The error occurred while setting parameters
### SQL: insert into sys_role(            role_name,       role_key,       role_sort,             menu_check_strictly,       dept_check_strictly,       status,             create_by,      create_time    )values(            ?,       ?,       ?,             ?,       ?,       ?,             ?,      now()    )
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：235
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "role_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：235', '2021-06-07 17:39:20.984863');
INSERT INTO "public"."sys_oper_log" VALUES (42, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"admin":false,"params":{},"roleSort":"0","deptCheckStrictly":true,"createBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","deptIds":[],"menuIds":[2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 在字段 "role_id" 中空值违反了非空约束
  详细：失败, 行包含(null, 测试角色, abc, 0, null, t, t, 0, null, admin, 2021-06-07 17:41:25.50921, null, null, null).
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysRoleMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysRoleMapper.insertRole-Inline
### The error occurred while setting parameters
### SQL: insert into sys_role(            role_name,       role_key,       role_sort,             menu_check_strictly,       dept_check_strictly,       status,             create_by,      create_time    )values(            ?,       ?,       ?::INT,             ?,       ?,       ?,             ?,      now()    )
### Cause: org.postgresql.util.PSQLException: 错误: 在字段 "role_id" 中空值违反了非空约束
  详细：失败, 行包含(null, 测试角色, abc, 0, null, t, t, 0, null, admin, 2021-06-07 17:41:25.50921, null, null, null).
; 错误: 在字段 "role_id" 中空值违反了非空约束
  详细：失败, 行包含(null, 测试角色, abc, 0, null, t, t, 0, null, admin, 2021-06-07 17:41:25.50921, null, null, null).; nested exception is org.postgresql.util.PSQLException: 错误: 在字段 "role_id" 中空值违反了非空约束
  详细：失败, 行包含(null, 测试角色, abc, 0, null, t, t, 0, null, admin, 2021-06-07 17:41:25.50921, null, null, null).', '2021-06-07 17:41:25.583792');
INSERT INTO "public"."sys_oper_log" VALUES (43, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"admin":false,"params":{},"roleSort":"0","deptCheckStrictly":true,"createBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","deptIds":[],"menuIds":[2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 重复键违反唯一约束"sys_role_pkey"
  详细：键值"(role_id)=(1)" 已经存在
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysRoleMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysRoleMapper.insertRole-Inline
### The error occurred while setting parameters
### SQL: insert into sys_role(            role_name,       role_key,       role_sort,             menu_check_strictly,       dept_check_strictly,       status,             create_by,      create_time    )values(            ?,       ?,       ?::INT,             ?,       ?,       ?,             ?,      now()    )
### Cause: org.postgresql.util.PSQLException: 错误: 重复键违反唯一约束"sys_role_pkey"
  详细：键值"(role_id)=(1)" 已经存在
; 错误: 重复键违反唯一约束"sys_role_pkey"
  详细：键值"(role_id)=(1)" 已经存在; nested exception is org.postgresql.util.PSQLException: 错误: 重复键违反唯一约束"sys_role_pkey"
  详细：键值"(role_id)=(1)" 已经存在', '2021-06-07 17:42:14.838781');
INSERT INTO "public"."sys_oper_log" VALUES (44, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":3,"admin":false,"params":{},"roleSort":"0","deptCheckStrictly":true,"createBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","deptIds":[],"menuIds":[2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:43:40.602431');
INSERT INTO "public"."sys_oper_log" VALUES (45, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"params":{},"roleSort":"0","deptCheckStrictly":true,"createBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","deptIds":[],"menuIds":[2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:46:44.735777');
INSERT INTO "public"."sys_oper_log" VALUES (46, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","menuIds":[2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:47:14.656044');
INSERT INTO "public"."sys_oper_log" VALUES (47, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","menuIds":[2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:47:19.164139');
INSERT INTO "public"."sys_oper_log" VALUES (88, '用户管理', 3, 'com.ruoyi.web.controller.system.SysUserController.remove()', 'DELETE', 1, 'admin', NULL, '/system/user/4', '127.0.0.1', '内网IP', '{userIds=4}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:55.561496');
INSERT INTO "public"."sys_oper_log" VALUES (48, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色123","menuIds":[2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:47:25.70336');
INSERT INTO "public"."sys_oper_log" VALUES (49, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:47:31.804578');
INSERT INTO "public"."sys_oper_log" VALUES (50, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116,4],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:47:35.809609');
INSERT INTO "public"."sys_oper_log" VALUES (51, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:47:40.638064');
INSERT INTO "public"."sys_oper_log" VALUES (52, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"roles":[{"flag":false,"roleId":2,"admin":false,"dataScope":"2","params":{},"roleSort":"2","deptCheckStrictly":false,"menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","status":"0"}],"phonenumber":"18988888888","admin":false,"loginDate":1623055176125,"delFlag":"0","password":"","updateBy":"admin","postIds":[],"loginIp":"127.0.0.1","email":"18988888888@qq.com","nickName":"测试","sex":"0","deptId":109,"avatar":"/profile/avatar/2021/06/07/a6a509ed-5819-4176-983f-b1f00aeca6f6.jpeg","dept":{"deptName":"财务部门","leader":"若依","deptId":109,"orderNum":"2","params":{},"parentId":102,"children":[],"status":"0"},"params":{},"userName":"test","userId":4,"createBy":"admin","roleIds":[4],"createTime":1623051954000,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:47:48.26945');
INSERT INTO "public"."sys_oper_log" VALUES (53, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'test', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"test","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:48:31.533243');
INSERT INTO "public"."sys_oper_log" VALUES (54, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 17:49:08.687089');
INSERT INTO "public"."sys_oper_log" VALUES (55, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"system","orderNum":"1","menuName":"系统管理","params":{},"parentId":0,"isCache":"0","path":"system","children":[],"createTime":1622786229000,"updateBy":"admin","isFrame":"1","menuId":1,"menuType":"M","perms":"","status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：74
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysMenuMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysMenuMapper.updateMenu-Inline
### The error occurred while setting parameters
### SQL: update sys_menu    SET menu_name = ?,    parent_id = ?,    order_num = ?,    path = ?,        is_frame = ?,    is_cache = ?,    menu_type = ?,    visible = ?,    status = ?,    perms = ?,    icon = ?,        update_by = ?,    update_time = now()    where menu_id = ?
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：74
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：74', '2021-06-07 17:51:12.005579');
INSERT INTO "public"."sys_oper_log" VALUES (56, '角色管理', 5, 'com.ruoyi.web.controller.system.SysRoleController.export()', 'GET', 1, 'admin', NULL, '/system/role/export', '127.0.0.1', '内网IP', '{}', '{"msg":"6852a5df-9612-4ca8-8e5b-08d52b9f283c_角色数据.xlsx","code":200}', 0, NULL, '2021-06-07 20:38:09.473783');
INSERT INTO "public"."sys_oper_log" VALUES (57, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"params":{},"deptCheckStrictly":false,"updateBy":"admin","menuCheckStrictly":false,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:38:13.561445');
INSERT INTO "public"."sys_oper_log" VALUES (145, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 11:25:27.188055');
INSERT INTO "public"."sys_oper_log" VALUES (59, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"测试","params":{},"parentId":0,"isCache":"0","path":"/test","createBy":"admin","children":[],"isFrame":"1","menuType":"M","status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：220
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysMenuMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysMenuMapper.insertMenu-Inline
### The error occurred while setting parameters
### SQL: insert into sys_menu(            menu_name,     order_num,     path,         is_frame,     is_cache,     menu_type,     visible,     status,         icon,         create_by,    create_time   )values(            ?,     ?,     ?,         ?,     ?,     ?,     ?,     ?,         ?,         ?,    now()   )
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：220
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：220', '2021-06-07 20:39:35.887023');
INSERT INTO "public"."sys_oper_log" VALUES (60, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","createBy":"admin","children":[],"isFrame":"1","menuType":"M","status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "is_frame" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：245
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysMenuMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysMenuMapper.insertMenu-Inline
### The error occurred while setting parameters
### SQL: insert into sys_menu(            menu_name,     order_num,     path,         is_frame,     is_cache,     menu_type,     visible,     status,         icon,         create_by,    create_time   )values(            ?,     ?::INT,     ?,         ?,     ?,     ?,     ?,     ?,         ?,         ?,    now()   )
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "is_frame" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：245
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "is_frame" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：245', '2021-06-07 20:45:45.261205');
INSERT INTO "public"."sys_oper_log" VALUES (61, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","createBy":"admin","children":[],"isFrame":"1","menuType":"M","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:47:35.429544');
INSERT INTO "public"."sys_oper_log" VALUES (62, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","children":[],"createTime":1623070055000,"updateBy":"admin","isFrame":"1","menuId":1061,"menuType":"M","perms":"","status":"1"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：74
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysMenuMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysMenuMapper.updateMenu-Inline
### The error occurred while setting parameters
### SQL: update sys_menu    SET menu_name = ?,    parent_id = ?,    order_num = ?,    path = ?,        is_frame = ?,    is_cache = ?,    menu_type = ?,    visible = ?,    status = ?,    perms = ?,    icon = ?,        update_by = ?,    update_time = now()    where menu_id = ?
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：74
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "order_num" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：74', '2021-06-07 20:47:40.926868');
INSERT INTO "public"."sys_oper_log" VALUES (63, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","children":[],"createTime":1623070055000,"updateBy":"admin","isFrame":"1","menuId":1061,"menuType":"M","perms":"","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:48:40.929345');
INSERT INTO "public"."sys_oper_log" VALUES (64, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"1","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","children":[],"createTime":1623070055000,"updateBy":"admin","isFrame":"1","menuId":1061,"menuType":"M","perms":"","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:48:54.991927');
INSERT INTO "public"."sys_oper_log" VALUES (65, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","children":[],"createTime":1623070055000,"updateBy":"admin","isFrame":"1","menuId":1061,"menuType":"M","perms":"","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:48:59.992502');
INSERT INTO "public"."sys_oper_log" VALUES (66, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","children":[],"createTime":1623070055000,"updateBy":"admin","isFrame":"1","menuId":1061,"menuType":"F","perms":"","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:49:08.298639');
INSERT INTO "public"."sys_oper_log" VALUES (67, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","children":[],"createTime":1623070055000,"updateBy":"admin","isFrame":"1","menuId":1061,"menuType":"C","perms":"","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:49:14.350552');
INSERT INTO "public"."sys_oper_log" VALUES (104, '参数管理', 2, 'com.ruoyi.web.controller.system.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{"configName":"test1111","configKey":"testdfdff","createBy":"admin","createTime":1623071534000,"updateBy":"admin","configId":4,"configType":"Y","configValue":"testdsfds","params":{}}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:12:24.005105');
INSERT INTO "public"."sys_oper_log" VALUES (68, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","component":"ss","children":[],"createTime":1623070055000,"updateBy":"admin","isFrame":"1","menuId":1061,"menuType":"C","perms":"ss","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:49:21.357951');
INSERT INTO "public"."sys_oper_log" VALUES (69, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"build","orderNum":"100","menuName":"test","params":{},"parentId":0,"isCache":"0","path":"/test","component":"ss","children":[],"createTime":1623070055000,"updateBy":"admin","isFrame":"1","menuId":1061,"menuType":"M","perms":"ss","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:49:26.587026');
INSERT INTO "public"."sys_oper_log" VALUES (70, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","orderNum":"3242","menuName":"sdf","params":{},"parentId":1061,"isCache":"0","path":"fdg","createBy":"admin","children":[],"isFrame":"1","menuType":"M","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:49:37.464881');
INSERT INTO "public"."sys_oper_log" VALUES (71, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{"visible":"0","icon":"#","orderNum":"3242","menuName":"sdf","params":{},"parentId":1061,"isCache":"0","path":"fdg","children":[],"createTime":1623070177000,"updateBy":"admin","isFrame":"1","menuId":1062,"menuType":"M","perms":"","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:49:44.195481');
INSERT INTO "public"."sys_oper_log" VALUES (72, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1062', '127.0.0.1', '内网IP', '{menuId=1062}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:49:46.279856');
INSERT INTO "public"."sys_oper_log" VALUES (73, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1061', '127.0.0.1', '内网IP', '{menuId=1061}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:49:49.205078');
INSERT INTO "public"."sys_oper_log" VALUES (74, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.add()', 'POST', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{"deptName":"TEST部门","leader":"zc","orderNum":"100","params":{},"parentId":100,"createBy":"admin","children":[],"phone":"18988888888","ancestors":"0,100","email":"18988888888@qq.com","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:55:36.751961');
INSERT INTO "public"."sys_oper_log" VALUES (75, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{"deptName":"TEST部门dd","leader":"zc","deptId":110,"orderNum":"100","delFlag":"0","params":{},"parentId":100,"createBy":"admin","children":[],"createTime":1623070536000,"phone":"18988888888","updateBy":"admin","ancestors":"0,100","email":"18988888888@qq.com","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:55:48.930631');
INSERT INTO "public"."sys_oper_log" VALUES (76, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{"deptName":"TEST部门","leader":"zc","deptId":110,"orderNum":"100","delFlag":"0","params":{},"parentId":100,"createBy":"admin","children":[],"createTime":1623070536000,"phone":"18988888888","updateBy":"admin","ancestors":"0,100","email":"18988888888@qq.com","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:55:54.065949');
INSERT INTO "public"."sys_oper_log" VALUES (77, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.add()', 'POST', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{"deptName":"dgsf","orderNum":"120","params":{},"parentId":110,"createBy":"admin","children":[],"ancestors":"0,100,110","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:01.603714');
INSERT INTO "public"."sys_oper_log" VALUES (78, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{"deptName":"dgsf","deptId":111,"orderNum":"120","delFlag":"0","params":{},"parentId":110,"createBy":"admin","children":[],"createTime":1623070561000,"updateBy":"admin","ancestors":"0,100,110","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:05.526673');
INSERT INTO "public"."sys_oper_log" VALUES (79, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{"deptName":"dgsf","deptId":111,"orderNum":"120","delFlag":"0","params":{},"parentId":110,"createBy":"admin","children":[],"createTime":1623070561000,"updateBy":"admin","ancestors":"0,100,110","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:09.487419');
INSERT INTO "public"."sys_oper_log" VALUES (80, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/111', '127.0.0.1', '内网IP', '{deptId=111}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:11.080726');
INSERT INTO "public"."sys_oper_log" VALUES (81, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/110', '127.0.0.1', '内网IP', '{deptId=110}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:13.277022');
INSERT INTO "public"."sys_oper_log" VALUES (82, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/4', '127.0.0.1', '内网IP', '{roleIds=4}', 'null', 1, '测试角色已分配,不能删除', '2021-06-07 20:56:19.808016');
INSERT INTO "public"."sys_oper_log" VALUES (83, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{"flag":false,"roleId":4,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623059204000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"abc","roleName":"测试角色","menuIds":[],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:27.765943');
INSERT INTO "public"."sys_oper_log" VALUES (84, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/4', '127.0.0.1', '内网IP', '{roleIds=4}', 'null', 1, '测试角色已分配,不能删除', '2021-06-07 20:56:30.32717');
INSERT INTO "public"."sys_oper_log" VALUES (85, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{"roles":[{"flag":false,"roleId":4,"admin":false,"dataScope":"1","params":{},"roleSort":"0","deptCheckStrictly":false,"menuCheckStrictly":false,"roleKey":"abc","roleName":"测试角色","status":"0"}],"phonenumber":"18988888888","admin":false,"loginDate":1623059288665,"delFlag":"0","password":"","updateBy":"admin","postIds":[],"loginIp":"127.0.0.1","email":"18988888888@qq.com","nickName":"测试","sex":"0","deptId":109,"avatar":"/profile/avatar/2021/06/07/a6a509ed-5819-4176-983f-b1f00aeca6f6.jpeg","dept":{"deptName":"财务部门","leader":"若依","deptId":109,"orderNum":"2","params":{},"parentId":102,"children":[],"status":"0"},"params":{},"userName":"test","userId":4,"createBy":"admin","roleIds":[],"createTime":1623051954000,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:38.431365');
INSERT INTO "public"."sys_oper_log" VALUES (86, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/4', '127.0.0.1', '内网IP', '{roleIds=4}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:44.427283');
INSERT INTO "public"."sys_oper_log" VALUES (87, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.resetPwd()', 'PUT', 1, 'admin', NULL, '/system/user/resetPwd', '127.0.0.1', '内网IP', '{"admin":false,"password":"$2a$10$Vfg/bm74xDAGdUaGzC1.qeRB2LNQlPLtqQLavN8C.lOxAB1JpCSb.","updateBy":"admin","params":{},"userId":4}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:56:52.796475');
INSERT INTO "public"."sys_oper_log" VALUES (89, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{"deptName":"财务部门","leader":"若依","deptId":109,"orderNum":"2","delFlag":"0","params":{},"parentId":102,"createBy":"admin","children":[],"createTime":1622786229000,"phone":"15888888888","updateBy":"admin","ancestors":"0,100,102","email":"ry@qq.com","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:57:36.161339');
INSERT INTO "public"."sys_oper_log" VALUES (90, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{"deptName":"财务部门","leader":"若依","deptId":109,"orderNum":"2","delFlag":"0","params":{},"parentId":102,"createBy":"admin","children":[],"createTime":1622786229000,"phone":"15888888888","updateBy":"admin","ancestors":"0,100,102","email":"ry@qq.com","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 20:57:41.026431');
INSERT INTO "public"."sys_oper_log" VALUES (91, '岗位管理', 1, 'com.ruoyi.web.controller.system.SysPostController.add()', 'POST', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{"postSort":"0","flag":false,"params":{},"createBy":"admin","postName":"JAVA","postCode":"0001","status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "post_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：176
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysPostMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysPostMapper.insertPost-Inline
### The error occurred while setting parameters
### SQL: insert into sys_post(            post_code,       post_name,       post_sort,       status,             create_by,      create_time    )values(            ?,       ?,       ?,       ?,             ?,      now()    )
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "post_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：176
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "post_sort" 的类型为 integer, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：176', '2021-06-07 20:58:33.539919');
INSERT INTO "public"."sys_oper_log" VALUES (92, '岗位管理', 1, 'com.ruoyi.web.controller.system.SysPostController.add()', 'POST', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{"postSort":"0","flag":false,"postId":5,"params":{},"createBy":"admin","postName":"JAVA","postCode":"001","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:02:23.70159');
INSERT INTO "public"."sys_oper_log" VALUES (93, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.edit()', 'PUT', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{"postSort":"101","flag":false,"postId":5,"params":{},"createBy":"admin","createTime":1623070943000,"updateBy":"admin","postName":"JAVAaa","postCode":"001","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:02:30.977793');
INSERT INTO "public"."sys_oper_log" VALUES (94, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.edit()', 'PUT', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{"postSort":"101","flag":false,"postId":5,"params":{},"createBy":"admin","createTime":1623070943000,"updateBy":"admin","postName":"JAVA","postCode":"001","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:02:40.928412');
INSERT INTO "public"."sys_oper_log" VALUES (95, '岗位管理', 5, 'com.ruoyi.web.controller.system.SysPostController.export()', 'GET', 1, 'admin', NULL, '/system/post/export', '127.0.0.1', '内网IP', '{}', '{"msg":"c3cdca9f-fd92-48da-9842-36c1d5cc6354_岗位数据.xlsx","code":200}', 0, NULL, '2021-06-07 21:02:44.231842');
INSERT INTO "public"."sys_oper_log" VALUES (96, '岗位管理', 3, 'com.ruoyi.web.controller.system.SysPostController.remove()', 'DELETE', 1, 'admin', NULL, '/system/post/5', '127.0.0.1', '内网IP', '{postIds=5}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:02:47.280669');
INSERT INTO "public"."sys_oper_log" VALUES (97, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.edit()', 'PUT', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{"postSort":"4","flag":false,"remark":"","postId":4,"params":{},"createBy":"admin","createTime":1622786229000,"updateBy":"admin","postName":"普通员工","postCode":"user","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:03:26.158453');
INSERT INTO "public"."sys_oper_log" VALUES (98, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.edit()', 'PUT', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{"postSort":"4","flag":false,"remark":"","postId":4,"params":{},"createBy":"admin","createTime":1622786229000,"updateBy":"admin","postName":"普通员工","postCode":"user","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:03:36.719202');
INSERT INTO "public"."sys_oper_log" VALUES (99, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{"createBy":"admin","dictName":"test","params":{},"dictType":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:08:56.392152');
INSERT INTO "public"."sys_oper_log" VALUES (100, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{"createBy":"admin","createTime":1623071336000,"updateBy":"admin","dictName":"test","dictId":11,"params":{},"dictType":"test","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:09:04.348514');
INSERT INTO "public"."sys_oper_log" VALUES (101, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dict/type/11', '127.0.0.1', '内网IP', '{dictIds=11}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:09:29.922013');
INSERT INTO "public"."sys_oper_log" VALUES (102, '参数管理', 1, 'com.ruoyi.web.controller.system.SysConfigController.add()', 'POST', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{"configName":"test","configKey":"test","createBy":"admin","configType":"Y","configValue":"test","params":{}}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 在字段 "config_id" 中空值违反了非空约束
  详细：失败, 行包含(null, test, test, test, Y, admin, 2021-06-07 21:09:58.177519, null, null, null).
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysConfigMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysConfigMapper.insertConfig-Inline
### The error occurred while setting parameters
### SQL: insert into sys_config (          config_name,           config_key,           config_value,           config_type,           create_by,                    create_time         )values(          ?,           ?,           ?,           ?,           ?,                    now()         )
### Cause: org.postgresql.util.PSQLException: 错误: 在字段 "config_id" 中空值违反了非空约束
  详细：失败, 行包含(null, test, test, test, Y, admin, 2021-06-07 21:09:58.177519, null, null, null).
; 错误: 在字段 "config_id" 中空值违反了非空约束
  详细：失败, 行包含(null, test, test, test, Y, admin, 2021-06-07 21:09:58.177519, null, null, null).; nested exception is org.postgresql.util.PSQLException: 错误: 在字段 "config_id" 中空值违反了非空约束
  详细：失败, 行包含(null, test, test, test, Y, admin, 2021-06-07 21:09:58.177519, null, null, null).', '2021-06-07 21:09:58.232466');
INSERT INTO "public"."sys_oper_log" VALUES (103, '参数管理', 1, 'com.ruoyi.web.controller.system.SysConfigController.add()', 'POST', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{"configName":"test","configKey":"test","createBy":"admin","configType":"Y","configValue":"test","params":{}}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:12:14.93873');
INSERT INTO "public"."sys_oper_log" VALUES (105, '参数管理', 2, 'com.ruoyi.web.controller.system.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{"configName":"test1111","configKey":"testdfdff","createBy":"admin","createTime":1623071534000,"updateBy":"admin","configId":4,"updateTime":1623071543000,"configType":"N","configValue":"testdsfds","params":{}}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:12:27.559656');
INSERT INTO "public"."sys_oper_log" VALUES (106, '参数管理', 2, 'com.ruoyi.web.controller.system.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{"configName":"test1111","configKey":"testdfdff","createBy":"admin","createTime":1623071534000,"updateBy":"admin","configId":4,"remark":"dsfsdggf","updateTime":1623071547000,"configType":"N","configValue":"testdsfds","params":{}}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:12:31.201459');
INSERT INTO "public"."sys_oper_log" VALUES (107, '参数管理', 3, 'com.ruoyi.web.controller.system.SysConfigController.remove()', 'DELETE', 1, 'admin', NULL, '/system/config/4', '127.0.0.1', '内网IP', '{configIds=4}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:12:59.522545');
INSERT INTO "public"."sys_oper_log" VALUES (108, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>sfsdfsdf大师傅大师傅官方</p>","createBy":"admin","noticeType":"1","params":{},"noticeTitle":"test","status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 字段 "notice_content" 的类型为 bytea, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：175
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysNoticeMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysNoticeMapper.insertNotice-Inline
### The error occurred while setting parameters
### SQL: insert into sys_notice (     notice_title,       notice_type,       notice_content,       status,             create_by,      create_time    )values(     ?,       ?,       ?,       ?,             ?,      now()   )
### Cause: org.postgresql.util.PSQLException: 错误: 字段 "notice_content" 的类型为 bytea, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：175
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 字段 "notice_content" 的类型为 bytea, 但表达式的类型为 character varying
  建议：你需要重写或转换表达式
  位置：175', '2021-06-07 21:14:55.478632');
INSERT INTO "public"."sys_oper_log" VALUES (109, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>dsaf梵蒂冈豆腐干反对是豆腐干山豆根</p>","createBy":"admin","noticeType":"1","params":{},"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:17:30.835967');
INSERT INTO "public"."sys_oper_log" VALUES (110, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>\\</p>","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"2","params":{},"noticeId":3,"noticeTitle":"test","status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 类型bytea的输入语法无效
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-system\target\classes\mapper\system\SysNoticeMapper.xml]
### The error may involve com.ruoyi.system.mapper.SysNoticeMapper.updateNotice-Inline
### The error occurred while setting parameters
### SQL: update sys_notice           SET notice_title = ?,              notice_type = ?,              notice_content = ?::BYTEA,              status = ?,              update_by = ?,     update_time = now()          where notice_id = ?
### Cause: org.postgresql.util.PSQLException: 错误: 类型bytea的输入语法无效
; 错误: 类型bytea的输入语法无效; nested exception is org.postgresql.util.PSQLException: 错误: 类型bytea的输入语法无效', '2021-06-07 21:17:35.346509');
INSERT INTO "public"."sys_oper_log" VALUES (111, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>大师傅士大夫但是</p>","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"1","params":{},"noticeId":3,"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 21:18:21.906391');
INSERT INTO "public"."sys_oper_log" VALUES (112, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"上的飞机","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"1","updateTime":1623071901000,"params":{},"noticeId":4,"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:11:58.847484');
INSERT INTO "public"."sys_oper_log" VALUES (113, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>上的飞机</p>","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"1","updateTime":1623075118000,"params":{},"noticeId":4,"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:12:01.706506');
INSERT INTO "public"."sys_oper_log" VALUES (114, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>维护内容</p>","createBy":"admin","createTime":1622786230000,"updateBy":"admin","noticeType":"1","remark":"管理员","params":{},"noticeId":2,"noticeTitle":"维护通知：2018-07-01 若依系统凌晨维护","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:12:05.168946');
INSERT INTO "public"."sys_oper_log" VALUES (115, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>上的飞机</p>","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"1","updateTime":1623075121000,"params":{},"noticeId":4,"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:12:10.608861');
INSERT INTO "public"."sys_oper_log" VALUES (116, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>大师傅士大夫但是</p>","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"1","updateTime":1623071901000,"params":{},"noticeId":3,"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:12:12.987676');
INSERT INTO "public"."sys_oper_log" VALUES (117, '通知公告', 3, 'com.ruoyi.web.controller.system.SysNoticeController.remove()', 'DELETE', 1, 'admin', NULL, '/system/notice/3', '127.0.0.1', '内网IP', '{noticeIds=3}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:12:14.981436');
INSERT INTO "public"."sys_oper_log" VALUES (118, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p>dsfsdfsdfssdfsf撒旦发生发撒法</p>","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"1","updateTime":1623075130000,"params":{},"noticeId":4,"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:12:28.608726');
INSERT INTO "public"."sys_oper_log" VALUES (119, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p><strong class=\"ql-size-huge\">dsfsdfsdfssdfsf撒旦发生发撒法</strong></p>","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"1","updateTime":1623075148000,"params":{},"noticeId":4,"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:12:37.420798');
INSERT INTO "public"."sys_oper_log" VALUES (120, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{"noticeContent":"<p><strong class=\"ql-size-huge\">dsfsdfsdfssdfsf撒旦发生发撒法</strong></p>","createBy":"admin","createTime":1623071850000,"updateBy":"admin","noticeType":"1","updateTime":1623075157000,"params":{},"noticeId":4,"noticeTitle":"test","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:12:43.831737');
INSERT INTO "public"."sys_oper_log" VALUES (122, '操作日志', 3, 'com.ruoyi.web.controller.monitor.SysOperlogController.remove()', 'DELETE', 1, 'admin', NULL, '/monitor/operlog/121', '127.0.0.1', '内网IP', '{operIds=121}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:13:44.710242');
INSERT INTO "public"."sys_oper_log" VALUES (123, '在线用户', 7, 'com.ruoyi.web.controller.monitor.SysUserOnlineController.forceLogout()', 'DELETE', 1, NULL, NULL, '/monitor/online/48957b5d-a084-4eae-bbcf-d7a9a374a2ec', '127.0.0.1', '内网IP', '{tokenId=48957b5d-a084-4eae-bbcf-d7a9a374a2ec}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:15:48.809592');
INSERT INTO "public"."sys_oper_log" VALUES (124, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{"params":{},"jobId":1,"misfirePolicy":"0","status":"0"}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 函数 sysdate() 不存在
  建议：没有匹配指定名称和参数类型的函数. 您也许需要增加明确的类型转换.
  位置：200
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-quartz\target\classes\mapper\quartz\SysJobMapper.xml]
### The error may involve com.ruoyi.quartz.mapper.SysJobMapper.updateJob-Inline
### The error occurred while setting parameters
### SQL: update sys_job     SET job_name = ?,     job_group = ?,     invoke_target = ?,     cron_expression = ?,     misfire_policy = ?,     concurrent = ?,     status = ?,               update_time = sysdate()     where job_id = ?
### Cause: org.postgresql.util.PSQLException: 错误: 函数 sysdate() 不存在
  建议：没有匹配指定名称和参数类型的函数. 您也许需要增加明确的类型转换.
  位置：200
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 函数 sysdate() 不存在
  建议：没有匹配指定名称和参数类型的函数. 您也许需要增加明确的类型转换.
  位置：200', '2021-06-07 22:16:00.658373');
INSERT INTO "public"."sys_oper_log" VALUES (125, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{"params":{},"jobId":1,"misfirePolicy":"0","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:21:40.909633');
INSERT INTO "public"."sys_oper_log" VALUES (126, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{"params":{},"jobId":1,"misfirePolicy":"0","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:21:47.766786');
INSERT INTO "public"."sys_oper_log" VALUES (127, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":2,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:22:01.0099');
INSERT INTO "public"."sys_oper_log" VALUES (128, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":2,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:22:22.314563');
INSERT INTO "public"."sys_oper_log" VALUES (129, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":2,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:24:33.453595');
INSERT INTO "public"."sys_oper_log" VALUES (130, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:26:28.093212');
INSERT INTO "public"."sys_oper_log" VALUES (131, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:29:09.51522');
INSERT INTO "public"."sys_oper_log" VALUES (132, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:30:22.020752');
INSERT INTO "public"."sys_oper_log" VALUES (133, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:33:31.894927');
INSERT INTO "public"."sys_oper_log" VALUES (134, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:39:26.351109');
INSERT INTO "public"."sys_oper_log" VALUES (135, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:44:26.974439');
INSERT INTO "public"."sys_oper_log" VALUES (136, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:45:03.712562');
INSERT INTO "public"."sys_oper_log" VALUES (137, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-07 22:53:04.170938');
INSERT INTO "public"."sys_oper_log" VALUES (138, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 09:25:55.57133');
INSERT INTO "public"."sys_oper_log" VALUES (139, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 09:43:50.765007');
INSERT INTO "public"."sys_oper_log" VALUES (140, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 09:44:39.623687');
INSERT INTO "public"."sys_oper_log" VALUES (141, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 09:46:31.029237');
INSERT INTO "public"."sys_oper_log" VALUES (143, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{"params":{},"jobId":1,"misfirePolicy":"0","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 11:25:23.842084');
INSERT INTO "public"."sys_oper_log" VALUES (144, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{"params":{},"jobId":1,"misfirePolicy":"0","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 11:25:25.883695');
INSERT INTO "public"."sys_oper_log" VALUES (146, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', 'null', 1, 'Couldn''t store trigger ''DEFAULT.MT_3o9jab7n92mjp'' for ''DEFAULT.TASK_CLASS_NAME1'' job:The job (DEFAULT.TASK_CLASS_NAME1) referenced by the trigger does not exist.', '2021-06-08 11:26:33.811362');
INSERT INTO "public"."sys_oper_log" VALUES (147, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":3,"misfirePolicy":"0"}', 'null', 1, 'Couldn''t store trigger ''DEFAULT.MT_1aoev0ttw286r'' for ''DEFAULT.TASK_CLASS_NAME3'' job:The job (DEFAULT.TASK_CLASS_NAME3) referenced by the trigger does not exist.', '2021-06-08 11:27:09.995086');
INSERT INTO "public"."sys_oper_log" VALUES (148, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 11:27:51.696041');
INSERT INTO "public"."sys_oper_log" VALUES (149, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":3,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 11:27:58.949343');
INSERT INTO "public"."sys_oper_log" VALUES (150, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":2,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 11:28:03.320947');
INSERT INTO "public"."sys_oper_log" VALUES (151, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'test', 'null', 1, '导入失败：
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 在字段 "table_id" 中空值违反了非空约束
  详细：失败, 行包含(null, test, null, null, null, Test, null, com.ruoyi.system, system, test, null, ruoyi, null, null, null, admin, 2021-06-08 11:29:44.183203, null, null, null).
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableMapper.insertGenTable-Inline
### The error occurred while setting parameters
### SQL: insert into gen_table (     table_name,           class_name,           package_name,      module_name,      business_name,           function_author,                      create_by,     create_time          )values(     ?,           ?,           ?,      ?,      ?,           ?,                      ?,     now()          )
### Cause: org.postgresql.util.PSQLException: 错误: 在字段 "table_id" 中空值违反了非空约束
  详细：失败, 行包含(null, test, null, null, null, Test, null, com.ruoyi.system, system, test, null, ruoyi, null, null, null, admin, 2021-06-08 11:29:44.183203, null, null, null).
; 错误: 在字段 "table_id" 中空值违反了非空约束
  详细：失败, 行包含(null, test, null, null, null, Test, null, com.ruoyi.system, system, test, null, ruoyi, null, null, null, admin, 2021-06-08 11:29:44.183203, null, null, null).; nested exception is org.postgresql.util.PSQLException: 错误: 在字段 "table_id" 中空值违反了非空约束
  详细：失败, 行包含(null, test, null, null, null, Test, null, com.ruoyi.system, system, test, null, ruoyi, null, null, null, admin, 2021-06-08 11:29:44.183203, null, null, null).', '2021-06-08 11:29:44.239173');
INSERT INTO "public"."sys_oper_log" VALUES (152, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'test', 'null', 1, '导入失败：
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 在字段 "column_id" 中空值违反了非空约束
  详细：失败, 行包含(null, 1, a, null, bytea, String, a, 0, 0, null, 1, 1, 1, 1, null, null, null, 1, admin, 2021-06-08 11:31:25.220815, null, null).
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableColumnMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableColumnMapper.insertGenTableColumn-Inline
### The error occurred while setting parameters
### SQL: insert into gen_table_column (     table_id,      column_name,           column_type,      java_type,      java_field,      is_pk,      is_increment,           is_insert,      is_edit,      is_list,      is_query,                     sort,      create_by,     create_time          )values(     ?,      ?,           ?,      ?,      ?,      ?,      ?,           ?,      ?,      ?,      ?,                     ?,      ?,     now()          )
### Cause: org.postgresql.util.PSQLException: 错误: 在字段 "column_id" 中空值违反了非空约束
  详细：失败, 行包含(null, 1, a, null, bytea, String, a, 0, 0, null, 1, 1, 1, 1, null, null, null, 1, admin, 2021-06-08 11:31:25.220815, null, null).
; 错误: 在字段 "column_id" 中空值违反了非空约束
  详细：失败, 行包含(null, 1, a, null, bytea, String, a, 0, 0, null, 1, 1, 1, 1, null, null, null, 1, admin, 2021-06-08 11:31:25.220815, null, null).; nested exception is org.postgresql.util.PSQLException: 错误: 在字段 "column_id" 中空值违反了非空约束
  详细：失败, 行包含(null, 1, a, null, bytea, String, a, 0, 0, null, 1, 1, 1, 1, null, null, null, 1, admin, 2021-06-08 11:31:25.220815, null, null).', '2021-06-08 11:31:25.264979');
INSERT INTO "public"."sys_oper_log" VALUES (153, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'test', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 11:33:37.949571');
INSERT INTO "public"."sys_oper_log" VALUES (154, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableColumnMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableColumnMapper.deleteGenTableColumnByIds-Inline
### The error occurred while setting parameters
### SQL: delete from gen_table_column where table_id in           (               ?          )
### Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45', '2021-06-08 13:33:40.786437');
INSERT INTO "public"."sys_oper_log" VALUES (155, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableColumnMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableColumnMapper.deleteGenTableColumnByIds-Inline
### The error occurred while setting parameters
### SQL: delete from gen_table_column where table_id in           (               ?::BIGINT          )
### Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45', '2021-06-08 13:36:07.359143');
INSERT INTO "public"."sys_oper_log" VALUES (165, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'sys_config', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:00:31.409039');
INSERT INTO "public"."sys_oper_log" VALUES (184, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '192.168.28.1', '内网IP', '{"admin":false,"updateBy":"admin","params":{},"userId":2,"status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:48:04.274764');
INSERT INTO "public"."sys_oper_log" VALUES (156, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = integer
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableColumnMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableColumnMapper.deleteGenTableColumnByIds-Inline
### The error occurred while setting parameters
### SQL: delete from gen_table_column where table_id in           (               ?::INT          )
### Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = integer
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = integer
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45', '2021-06-08 13:37:42.686284');
INSERT INTO "public"."sys_oper_log" VALUES (157, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableColumnMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableColumnMapper.deleteGenTableColumnByIds-Inline
### The error occurred while setting parameters
### SQL: delete from gen_table_column where table_id in           (               ?          )
### Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45', '2021-06-08 13:39:32.08265');
INSERT INTO "public"."sys_oper_log" VALUES (158, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableColumnMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableColumnMapper.deleteGenTableColumnByIds-Inline
### The error occurred while setting parameters
### SQL: delete from gen_table_column where table_id in           (               ?          )
### Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45', '2021-06-08 13:42:02.915243');
INSERT INTO "public"."sys_oper_log" VALUES (159, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', 'null', 1, 'nested exception is org.apache.ibatis.builder.BuilderException: Error resolving JdbcType. Cause: java.lang.IllegalArgumentException: No enum constant org.apache.ibatis.type.JdbcType.varchar', '2021-06-08 13:44:07.170602');
INSERT INTO "public"."sys_oper_log" VALUES (160, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', 'null', 1, '
### Error updating database.  Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableColumnMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableColumnMapper.deleteGenTableColumnByIds-Inline
### The error occurred while setting parameters
### SQL: delete from gen_table_column where table_id in           (               ?          )
### Cause: org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 操作符不存在: character varying = bigint
  建议：没有匹配指定名称和参数类型的操作符. 您也许需要增加明确的类型转换.
  位置：45', '2021-06-08 13:45:47.18856');
INSERT INTO "public"."sys_oper_log" VALUES (161, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', 'null', 1, 'nested exception is org.apache.ibatis.builder.BuilderException: Error resolving JdbcType. Cause: java.lang.IllegalArgumentException: No enum constant org.apache.ibatis.type.JdbcType.String', '2021-06-08 13:46:36.242919');
INSERT INTO "public"."sys_oper_log" VALUES (162, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 13:47:14.213873');
INSERT INTO "public"."sys_oper_log" VALUES (163, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'test', 'null', 1, '导入失败：
### Error querying database.  Cause: org.postgresql.util.PSQLException: 错误: 语法错误 在 "!=" 或附近的
  位置：70
### The error may exist in file [D:\Workspace\JavaWorkspace\ChinaTelecom\RuoYi-Vue\ruoyi-generator\target\classes\mapper\generator\GenTableColumnMapper.xml]
### The error may involve com.ruoyi.generator.mapper.GenTableColumnMapper.selectDbTableColumnsByName-Inline
### The error occurred while setting parameters
### SQL: select column_name, (case when (is_nullable = ''no''   &&   column_key != ''PRI'') then ''1'' else null end) as is_required, (case when column_key = ''PRI'' then ''1'' else ''0'' end) as is_pk, ordinal_position as sort, column_comment, (case when extra = ''auto_increment'' then ''1'' else ''0'' end) as is_increment, column_type   from information_schema.columns where table_schema = (select database()) and table_name = (?)   order by ordinal_position
### Cause: org.postgresql.util.PSQLException: 错误: 语法错误 在 "!=" 或附近的
  位置：70
; bad SQL grammar []; nested exception is org.postgresql.util.PSQLException: 错误: 语法错误 在 "!=" 或附近的
  位置：70', '2021-06-08 13:47:32.771104');
INSERT INTO "public"."sys_oper_log" VALUES (164, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'test', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 13:51:34.310102');
INSERT INTO "public"."sys_oper_log" VALUES (166, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{"sub":false,"functionAuthor":"ruoyi","columns":[{"capJavaField":"ConfigId","usableColumn":false,"columnId":3,"isIncrement":"1","increment":true,"insert":true,"required":false,"superColumn":false,"isInsert":"1","javaField":"configId","edit":false,"query":false,"columnComment":"参数主键","sort":1,"list":false,"params":{},"javaType":"String","queryType":"EQ","columnType":"int4","createBy":"admin","isPk":"1","createTime":1623132031000,"tableId":5,"pk":true,"columnName":"config_id"},{"capJavaField":"ConfigName","usableColumn":false,"columnId":4,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"configName","htmlType":"input","edit":true,"query":true,"columnComment":"参数名称","isQuery":"1","sort":2,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132031000,"isEdit":"1","tableId":5,"pk":false,"columnName":"config_name"},{"capJavaField":"ConfigKey","usableColumn":false,"columnId":5,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"configKey","htmlType":"input","edit":true,"query":true,"columnComment":"参数键名","isQuery":"1","sort":3,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132031000,"isEdit":"1","tableId":5,"pk":false,"columnName":"config_key"},{"capJavaField":"ConfigValue","usableColumn":false,"columnId":6,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"configValue","htmlType":"input","edit":true,"query":true,"columnComment":"参数键值","isQuery":"1","sort":4,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132031000,"isEdit":"1","tableId":5,"pk":false,"columnName":"config_value"},{"capJavaField":"ConfigT', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:00:38.410849');
INSERT INTO "public"."sys_oper_log" VALUES (167, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{"sub":false,"functionAuthor":"ruoyi","columns":[{"capJavaField":"ConfigId","usableColumn":false,"columnId":3,"isIncrement":"1","increment":true,"insert":true,"required":false,"superColumn":false,"isInsert":"1","javaField":"configId","edit":false,"query":false,"columnComment":"参数主键","updateTime":1623132038000,"sort":1,"list":false,"params":{},"javaType":"String","queryType":"EQ","columnType":"int4","createBy":"admin","isPk":"1","createTime":1623132031000,"tableId":5,"pk":true,"columnName":"config_id"},{"capJavaField":"ConfigName","usableColumn":false,"columnId":4,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"configName","htmlType":"input","edit":true,"query":true,"columnComment":"参数名称","isQuery":"1","updateTime":1623132038000,"sort":2,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132031000,"isEdit":"1","tableId":5,"pk":false,"columnName":"config_name"},{"capJavaField":"ConfigKey","usableColumn":false,"columnId":5,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"configKey","htmlType":"input","edit":true,"query":true,"columnComment":"参数键名","isQuery":"1","updateTime":1623132038000,"sort":3,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132031000,"isEdit":"1","tableId":5,"pk":false,"columnName":"config_key"},{"capJavaField":"ConfigValue","usableColumn":false,"columnId":6,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"configValue","htmlType":"input","edit":true,"query":true,"columnComment":"参数键值","isQuery":"1","updateTime":1623132038000,"sort":4,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"varchar","createBy":"admin","isPk":"0","createT', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:00:55.078287');
INSERT INTO "public"."sys_oper_log" VALUES (168, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/5', '127.0.0.1', '内网IP', '{tableIds=5}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:00:58.742428');
INSERT INTO "public"."sys_oper_log" VALUES (169, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/4', '127.0.0.1', '内网IP', '{tableIds=4}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:01:27.645777');
INSERT INTO "public"."sys_oper_log" VALUES (170, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'sys_user_copy1', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:01:55.856021');
INSERT INTO "public"."sys_oper_log" VALUES (171, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{"sub":false,"functionAuthor":"ruoyi","columns":[{"capJavaField":"UserId","usableColumn":false,"columnId":13,"isIncrement":"1","increment":true,"insert":true,"required":false,"superColumn":false,"isInsert":"1","javaField":"userId","edit":false,"query":false,"columnComment":"用户ID","sort":1,"list":false,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"1","createTime":1623132115000,"tableId":6,"pk":true,"columnName":"user_id"},{"capJavaField":"DeptId","usableColumn":false,"columnId":14,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"deptId","edit":true,"query":true,"columnComment":"部门ID","isQuery":"1","sort":2,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6,"pk":false,"columnName":"dept_id"},{"capJavaField":"UserName","usableColumn":false,"columnId":15,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"userName","htmlType":"input","edit":true,"query":true,"columnComment":"用户账号","isQuery":"1","sort":3,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6,"pk":false,"columnName":"user_name"},{"capJavaField":"NickName","usableColumn":false,"columnId":16,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"nickName","htmlType":"input","edit":true,"query":true,"columnComment":"用户昵称","isQuery":"1","sort":4,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6,"pk":false,"columnName":"nick_name"},{"capJavaField":"UserType","usableColumn":false,"columnId":17,"isIncre', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:02:06.825619');
INSERT INTO "public"."sys_oper_log" VALUES (172, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{"sub":false,"functionAuthor":"ruoyi","columns":[{"capJavaField":"UserId","usableColumn":false,"columnId":13,"isIncrement":"1","increment":true,"insert":true,"required":false,"superColumn":false,"isInsert":"1","javaField":"userId","edit":false,"query":false,"columnComment":"用户ID","updateTime":1623132126000,"sort":1,"list":false,"params":{},"javaType":"String","queryType":"NE","columnType":"int8","createBy":"admin","isPk":"1","createTime":1623132115000,"tableId":6,"pk":true,"columnName":"user_id"},{"capJavaField":"DeptId","usableColumn":false,"columnId":14,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"deptId","edit":true,"query":true,"columnComment":"部门ID","isQuery":"1","updateTime":1623132126000,"sort":2,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6,"pk":false,"columnName":"dept_id"},{"capJavaField":"UserName","usableColumn":false,"columnId":15,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"userName","htmlType":"input","edit":true,"query":true,"columnComment":"用户账号","isQuery":"1","updateTime":1623132126000,"sort":3,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6,"pk":false,"columnName":"user_name"},{"capJavaField":"NickName","usableColumn":false,"columnId":16,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"nickName","htmlType":"input","edit":true,"query":true,"columnComment":"用户昵称","isQuery":"1","updateTime":1623132126000,"sort":4,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:02:13.363595');
INSERT INTO "public"."sys_oper_log" VALUES (173, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{"sub":false,"functionAuthor":"ruoyi","columns":[{"capJavaField":"UserId","usableColumn":false,"columnId":13,"isIncrement":"1","increment":true,"insert":true,"required":false,"superColumn":false,"isInsert":"1","javaField":"userId","edit":false,"query":false,"columnComment":"用户ID","updateTime":1623132133000,"sort":1,"list":false,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"1","createTime":1623132115000,"tableId":6,"pk":true,"columnName":"user_id"},{"capJavaField":"DeptId","usableColumn":false,"columnId":14,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"deptId","edit":true,"query":true,"columnComment":"部门ID","isQuery":"1","updateTime":1623132133000,"sort":2,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6,"pk":false,"columnName":"dept_id"},{"capJavaField":"UserName","usableColumn":false,"columnId":15,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"userName","htmlType":"input","edit":true,"query":true,"columnComment":"用户账号","isQuery":"1","updateTime":1623132133000,"sort":3,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6,"pk":false,"columnName":"user_name"},{"capJavaField":"NickName","usableColumn":false,"columnId":16,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"nickName","htmlType":"input","edit":true,"query":true,"columnComment":"用户昵称","isQuery":"1","updateTime":1623132133000,"sort":4,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132115000,"isEdit":"1","tableId":6', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:02:19.336375');
INSERT INTO "public"."sys_oper_log" VALUES (174, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/sys_user_copy1', '127.0.0.1', '内网IP', '{tableName=sys_user_copy1}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:02:23.880914');
INSERT INTO "public"."sys_oper_log" VALUES (175, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-06-08 14:02:26.026234');
INSERT INTO "public"."sys_oper_log" VALUES (176, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/6', '127.0.0.1', '内网IP', '{tableIds=6}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:05:31.531178');
INSERT INTO "public"."sys_oper_log" VALUES (177, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'sys_user_copy1', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:05:40.488259');
INSERT INTO "public"."sys_oper_log" VALUES (178, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{"sub":false,"functionAuthor":"ruoyi","columns":[{"capJavaField":"UserId","usableColumn":false,"columnId":32,"isIncrement":"1","increment":true,"insert":true,"required":false,"superColumn":false,"isInsert":"1","javaField":"userId","edit":false,"query":false,"columnComment":"用户ID","sort":1,"list":false,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"1","createTime":1623132340000,"tableId":7,"pk":true,"columnName":"user_id"},{"capJavaField":"DeptId","usableColumn":false,"columnId":33,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"deptId","edit":true,"query":true,"columnComment":"部门ID","isQuery":"1","sort":2,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"0","createTime":1623132340000,"isEdit":"1","tableId":7,"pk":false,"columnName":"dept_id"},{"capJavaField":"UserName","usableColumn":false,"columnId":34,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"userName","htmlType":"input","edit":true,"query":true,"columnComment":"用户账号","isQuery":"1","sort":3,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132340000,"isEdit":"1","tableId":7,"pk":false,"columnName":"user_name"},{"capJavaField":"NickName","usableColumn":false,"columnId":35,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"nickName","htmlType":"input","edit":true,"query":true,"columnComment":"用户昵称","isQuery":"1","sort":4,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132340000,"isEdit":"1","tableId":7,"pk":false,"columnName":"nick_name"},{"capJavaField":"UserType","usableColumn":false,"columnId":36,"isIncre', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:05:45.22986');
INSERT INTO "public"."sys_oper_log" VALUES (179, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{"sub":false,"functionAuthor":"ruoyi","columns":[{"capJavaField":"UserId","usableColumn":false,"columnId":32,"isIncrement":"1","increment":true,"insert":true,"required":false,"superColumn":false,"isInsert":"1","javaField":"userId","edit":false,"query":false,"columnComment":"用户ID","updateTime":1623132345000,"sort":1,"list":false,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"1","createTime":1623132340000,"tableId":7,"pk":true,"columnName":"user_id"},{"capJavaField":"DeptId","usableColumn":false,"columnId":33,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"deptId","edit":true,"query":true,"columnComment":"部门ID","isQuery":"1","updateTime":1623132345000,"sort":2,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"int8","createBy":"admin","isPk":"0","createTime":1623132340000,"isEdit":"1","tableId":7,"pk":false,"columnName":"dept_id"},{"capJavaField":"UserName","usableColumn":false,"columnId":34,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"userName","htmlType":"input","edit":true,"query":true,"columnComment":"用户账号","isQuery":"1","updateTime":1623132345000,"sort":3,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132340000,"isEdit":"1","tableId":7,"pk":false,"columnName":"user_name"},{"capJavaField":"NickName","usableColumn":false,"columnId":35,"isIncrement":"0","increment":false,"insert":true,"isList":"1","required":false,"superColumn":false,"isInsert":"1","javaField":"nickName","htmlType":"input","edit":true,"query":true,"columnComment":"用户昵称","isQuery":"1","updateTime":1623132345000,"sort":4,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623132340000,"isEdit":"1","tableId":7', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:05:56.590539');
INSERT INTO "public"."sys_oper_log" VALUES (180, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/7', '127.0.0.1', '内网IP', '{tableIds=7}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:06:01.490605');
INSERT INTO "public"."sys_oper_log" VALUES (181, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'sys_user_copy1', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:06:09.986549');
INSERT INTO "public"."sys_oper_log" VALUES (182, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-06-08 14:06:11.844386');
INSERT INTO "public"."sys_oper_log" VALUES (183, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/8', '127.0.0.1', '内网IP', '{tableIds=8}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 14:06:15.355122');
INSERT INTO "public"."sys_oper_log" VALUES (185, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '192.168.28.1', '内网IP', '{"admin":false,"updateBy":"admin","params":{},"userId":2,"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:48:05.819224');
INSERT INTO "public"."sys_oper_log" VALUES (186, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '192.168.28.1', '内网IP', '{"flag":false,"roleId":2,"admin":false,"remark":"普通角色","dataScope":"2","delFlag":"0","params":{},"roleSort":"2","deptCheckStrictly":false,"createTime":1622786229000,"updateBy":"admin","menuCheckStrictly":false,"roleKey":"common","roleName":"普通角色","menuIds":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,1055,1056,1058,1057,1059,1060,4],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:02.846945');
INSERT INTO "public"."sys_oper_log" VALUES (187, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '192.168.28.1', '内网IP', '{"flag":false,"roleId":6,"admin":false,"params":{},"roleSort":"0","deptCheckStrictly":true,"createBy":"admin","menuCheckStrictly":true,"roleKey":"aa","roleName":"sa","deptIds":[],"menuIds":[],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:07.695804');
INSERT INTO "public"."sys_oper_log" VALUES (188, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '192.168.28.1', '内网IP', '{"flag":false,"roleId":6,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623138607000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"aa","roleName":"sa","menuIds":[3,114,115,1055,1056,1058,1057,1059,1060,116],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:12.285884');
INSERT INTO "public"."sys_oper_log" VALUES (189, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '192.168.28.1', '内网IP', '{"flag":false,"roleId":6,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623138607000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"aa","roleName":"sa","menuIds":[3,115,1055,1056,1058,1057,1059,1060],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:20.294066');
INSERT INTO "public"."sys_oper_log" VALUES (190, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '192.168.28.1', '内网IP', '{"flag":false,"roleId":6,"admin":false,"dataScope":"1","delFlag":"0","params":{},"roleSort":"0","deptCheckStrictly":true,"createTime":1623138607000,"updateBy":"admin","menuCheckStrictly":true,"roleKey":"aa","roleName":"sa","menuIds":[3,115,1055,1056,1058,1057,1059,1060],"status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:25.226224');
INSERT INTO "public"."sys_oper_log" VALUES (191, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/6', '192.168.28.1', '内网IP', '{roleIds=6}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:31.539284');
INSERT INTO "public"."sys_oper_log" VALUES (192, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '192.168.28.1', '内网IP', '{"visible":"0","icon":"monitor","orderNum":"2","menuName":"系统监控","params":{},"parentId":0,"isCache":"0","path":"monitor","children":[],"createTime":1622786229000,"updateBy":"admin","isFrame":"1","menuId":2,"menuType":"M","perms":"","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:37.86676');
INSERT INTO "public"."sys_oper_log" VALUES (193, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '192.168.28.1', '内网IP', '{"deptName":"深圳总公司","leader":"若依","deptId":101,"orderNum":"1","delFlag":"0","params":{},"parentId":100,"createBy":"admin","children":[],"createTime":1622786229000,"phone":"15888888888","updateBy":"admin","ancestors":"0,100","email":"ry@qq.com","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:41.183672');
INSERT INTO "public"."sys_oper_log" VALUES (194, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.edit()', 'PUT', 1, 'admin', NULL, '/system/post', '192.168.28.1', '内网IP', '{"postSort":"1","flag":false,"remark":"","postId":1,"params":{},"createBy":"admin","createTime":1622786229000,"updateBy":"admin","postName":"董事长","postCode":"ceo","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:44.667281');
INSERT INTO "public"."sys_oper_log" VALUES (195, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '192.168.28.1', '内网IP', '{"createBy":"admin","createTime":1622786229000,"updateBy":"admin","dictName":"用户性别","remark":"用户性别列表","dictId":1,"params":{},"dictType":"sys_user_sex","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:47.475232');
INSERT INTO "public"."sys_oper_log" VALUES (196, '参数管理', 2, 'com.ruoyi.web.controller.system.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/system/config', '192.168.28.1', '内网IP', '{"configName":"主框架页-默认皮肤样式名称","configKey":"sys.index.skinName","createBy":"admin","createTime":1622786229000,"updateBy":"admin","configId":1,"remark":"蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow","configType":"Y","configValue":"skin-blue","params":{}}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:50.704923');
INSERT INTO "public"."sys_oper_log" VALUES (197, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '192.168.28.1', '内网IP', '{"noticeContent":"<p>维护内容</p>","createBy":"admin","createTime":1622786230000,"updateBy":"admin","noticeType":"1","remark":"管理员","updateTime":1623075125000,"params":{},"noticeId":2,"noticeTitle":"维护通知：2018-07-01 若依系统凌晨维护","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:50:54.039706');
INSERT INTO "public"."sys_oper_log" VALUES (198, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '192.168.28.1', '内网IP', '{"noticeContent":"<p>维护内容dffsd大师傅似的</p>","createBy":"admin","createTime":1622786230000,"updateBy":"admin","noticeType":"1","remark":"管理员","updateTime":1623138654000,"params":{},"noticeId":2,"noticeTitle":"维护通知：2018-07-01 若依系统凌晨维护","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:51:01.064305');
INSERT INTO "public"."sys_oper_log" VALUES (199, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '192.168.28.1', '内网IP', '{"noticeContent":"<p>维护内容</p>","createBy":"admin","createTime":1622786230000,"updateBy":"admin","noticeType":"1","remark":"管理员","updateTime":1623138661000,"params":{},"noticeId":2,"noticeTitle":"维护通知：2018-07-01 若依系统凌晨维护","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:51:04.89015');
INSERT INTO "public"."sys_oper_log" VALUES (200, '登录日志', 3, 'com.ruoyi.web.controller.monitor.SysLogininforController.remove()', 'DELETE', 1, 'admin', NULL, '/monitor/logininfor/71', '192.168.28.1', '内网IP', '{infoIds=71}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:51:21.977561');
INSERT INTO "public"."sys_oper_log" VALUES (201, '登录日志', 5, 'com.ruoyi.web.controller.monitor.SysLogininforController.export()', 'GET', 1, 'admin', NULL, '/monitor/logininfor/export', '192.168.28.1', '内网IP', '{}', '{"msg":"ea6d3d48-8812-479b-9a3b-4aef06ec96cb_登录日志.xlsx","code":200}', 0, NULL, '2021-06-08 15:51:25.002607');
INSERT INTO "public"."sys_oper_log" VALUES (202, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '192.168.28.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":1,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:51:32.360596');
INSERT INTO "public"."sys_oper_log" VALUES (204, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '192.168.28.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":2,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:51:54.335203');
INSERT INTO "public"."sys_oper_log" VALUES (205, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '192.168.28.1', '内网IP', '{"params":{},"jobId":3,"misfirePolicy":"0","status":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:52:24.095568');
INSERT INTO "public"."sys_oper_log" VALUES (203, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '192.168.28.1', '内网IP', '{"jobGroup":"DEFAULT","params":{},"jobId":3,"misfirePolicy":"0"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:51:48.131155');
INSERT INTO "public"."sys_oper_log" VALUES (206, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '192.168.28.1', '内网IP', '{"params":{},"jobId":3,"misfirePolicy":"0","status":"1"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:53:33.202474');
INSERT INTO "public"."sys_oper_log" VALUES (207, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '192.168.28.1', '内网IP', 'demo_user', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:54:43.731859');
INSERT INTO "public"."sys_oper_log" VALUES (208, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '192.168.28.1', '内网IP', '{"sub":false,"functionAuthor":"ruoyi","columns":[{"capJavaField":"Id","usableColumn":false,"columnId":70,"isIncrement":"0","increment":false,"insert":true,"isList":"1","dictType":"sys_show_hide","required":true,"superColumn":false,"isInsert":"1","isRequired":"1","javaField":"id","htmlType":"textarea","edit":true,"query":true,"columnComment":"adbc","isQuery":"1","sort":1,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"int4","createBy":"admin","isPk":"1","createTime":1623138883000,"isEdit":"1","tableId":9,"pk":true,"columnName":"id"},{"capJavaField":"Name","usableColumn":false,"columnId":71,"isIncrement":"0","increment":false,"insert":true,"isList":"1","dictType":"sys_show_hide","required":true,"superColumn":false,"isInsert":"1","isRequired":"1","javaField":"name","htmlType":"input","edit":true,"query":true,"columnComment":"adbc","isQuery":"1","sort":2,"list":true,"params":{},"javaType":"String","queryType":"LIKE","columnType":"varchar","createBy":"admin","isPk":"0","createTime":1623138883000,"isEdit":"1","tableId":9,"pk":false,"columnName":"name"},{"capJavaField":"Age","usableColumn":false,"columnId":72,"isIncrement":"0","increment":false,"insert":true,"isList":"1","dictType":"sys_show_hide","required":true,"superColumn":false,"isInsert":"1","isRequired":"1","javaField":"age","htmlType":"input","edit":true,"query":true,"columnComment":"adbc","isQuery":"1","sort":3,"list":true,"params":{},"javaType":"String","queryType":"EQ","columnType":"int4","createBy":"admin","isPk":"0","createTime":1623138883000,"isEdit":"1","tableId":9,"pk":false,"columnName":"age"}],"businessName":"user","moduleName":"system","className":"DemoUser","tableName":"demo_user","crud":true,"options":"{}","genType":"0","packageName":"com.ruoyi.system","functionName":"aaa","tree":false,"tableComment":"aaa","params":{},"tplCategory":"crud","tableId":9,"genPath":"/"}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:55:36.460944');
INSERT INTO "public"."sys_oper_log" VALUES (209, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/9', '192.168.28.1', '内网IP', '{tableIds=9}', '{"msg":"操作成功","code":200}', 0, NULL, '2021-06-08 15:55:39.572454');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_post";
CREATE TABLE "public"."sys_post" (
  "post_id" int8 NOT NULL DEFAULT nextval('post_id_seq'::regclass),
  "post_code" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "post_name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "post_sort" int4 NOT NULL,
  "status" char(1) COLLATE "pg_catalog"."default" NOT NULL,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_post"."post_id" IS '岗位ID';
COMMENT ON COLUMN "public"."sys_post"."post_code" IS '岗位编码';
COMMENT ON COLUMN "public"."sys_post"."post_name" IS '岗位名称';
COMMENT ON COLUMN "public"."sys_post"."post_sort" IS '显示顺序';
COMMENT ON COLUMN "public"."sys_post"."status" IS '状态（0正常 1停用）';
COMMENT ON COLUMN "public"."sys_post"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_post"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_post"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_post"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_post"."remark" IS '备注';
COMMENT ON TABLE "public"."sys_post" IS '岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO "public"."sys_post" VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_post" VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2021-06-04 13:57:09', '', NULL, '');
INSERT INTO "public"."sys_post" VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-07 21:03:36.704664', '');
INSERT INTO "public"."sys_post" VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:44.646773', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_role";
CREATE TABLE "public"."sys_role" (
  "role_id" int8 NOT NULL DEFAULT nextval('role_id_seq'::regclass),
  "role_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "role_key" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "role_sort" int4 NOT NULL,
  "data_scope" char(1) COLLATE "pg_catalog"."default" DEFAULT '1'::bpchar,
  "menu_check_strictly" bool DEFAULT true,
  "dept_check_strictly" bool DEFAULT true,
  "status" char(1) COLLATE "pg_catalog"."default" NOT NULL,
  "del_flag" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_role"."role_id" IS '角色ID';
COMMENT ON COLUMN "public"."sys_role"."role_name" IS '角色名称';
COMMENT ON COLUMN "public"."sys_role"."role_key" IS '角色权限字符串';
COMMENT ON COLUMN "public"."sys_role"."role_sort" IS '显示顺序';
COMMENT ON COLUMN "public"."sys_role"."data_scope" IS '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）';
COMMENT ON COLUMN "public"."sys_role"."menu_check_strictly" IS '菜单树选择项是否关联显示';
COMMENT ON COLUMN "public"."sys_role"."dept_check_strictly" IS '部门树选择项是否关联显示';
COMMENT ON COLUMN "public"."sys_role"."status" IS '角色状态（0正常 1停用）';
COMMENT ON COLUMN "public"."sys_role"."del_flag" IS '删除标志（0代表存在 2代表删除）';
COMMENT ON COLUMN "public"."sys_role"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_role"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_role"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_role"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_role"."remark" IS '备注';
COMMENT ON TABLE "public"."sys_role" IS '角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO "public"."sys_role" VALUES (1, '超级管理员', 'admin', 1, '1', 't', 't', '0', '0', 'admin', '2021-06-04 13:57:09', '', NULL, '超级管理员');
INSERT INTO "public"."sys_role" VALUES (4, '测试角色', 'abc', 0, '1', 't', 't', '0', '2', 'admin', '2021-06-07 17:46:44.691835', 'admin', '2021-06-07 20:56:27.74293', NULL);
INSERT INTO "public"."sys_role" VALUES (2, '普通角色', 'common', 2, '2', 'f', 'f', '0', '0', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:50:02.804119', '普通角色');
INSERT INTO "public"."sys_role" VALUES (6, 'sa', 'aa', 0, '1', 't', 't', '0', '2', 'admin', '2021-06-08 15:50:07.673328', 'admin', '2021-06-08 15:50:25.20464', NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_role_dept";
CREATE TABLE "public"."sys_role_dept" (
  "role_id" int8 NOT NULL,
  "dept_id" int8 NOT NULL
)
;
COMMENT ON COLUMN "public"."sys_role_dept"."role_id" IS '角色ID';
COMMENT ON COLUMN "public"."sys_role_dept"."dept_id" IS '部门ID';
COMMENT ON TABLE "public"."sys_role_dept" IS '角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO "public"."sys_role_dept" VALUES (2, 100);
INSERT INTO "public"."sys_role_dept" VALUES (2, 101);
INSERT INTO "public"."sys_role_dept" VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_role_menu";
CREATE TABLE "public"."sys_role_menu" (
  "role_id" int8 NOT NULL,
  "menu_id" int8 NOT NULL
)
;
COMMENT ON COLUMN "public"."sys_role_menu"."role_id" IS '角色ID';
COMMENT ON COLUMN "public"."sys_role_menu"."menu_id" IS '菜单ID';
COMMENT ON TABLE "public"."sys_role_menu" IS '角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO "public"."sys_role_menu" VALUES (3, 2);
INSERT INTO "public"."sys_role_menu" VALUES (3, 109);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1046);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1047);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1048);
INSERT INTO "public"."sys_role_menu" VALUES (3, 110);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1049);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1050);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1051);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1052);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1053);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1054);
INSERT INTO "public"."sys_role_menu" VALUES (3, 111);
INSERT INTO "public"."sys_role_menu" VALUES (3, 112);
INSERT INTO "public"."sys_role_menu" VALUES (3, 113);
INSERT INTO "public"."sys_role_menu" VALUES (3, 3);
INSERT INTO "public"."sys_role_menu" VALUES (3, 114);
INSERT INTO "public"."sys_role_menu" VALUES (3, 115);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1055);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1056);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1058);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1057);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1059);
INSERT INTO "public"."sys_role_menu" VALUES (3, 1060);
INSERT INTO "public"."sys_role_menu" VALUES (3, 116);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1);
INSERT INTO "public"."sys_role_menu" VALUES (2, 100);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1001);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1002);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1003);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1004);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1005);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1006);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1007);
INSERT INTO "public"."sys_role_menu" VALUES (2, 101);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1008);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1009);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1010);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1011);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1012);
INSERT INTO "public"."sys_role_menu" VALUES (2, 102);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1013);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1014);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1015);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1016);
INSERT INTO "public"."sys_role_menu" VALUES (2, 103);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1017);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1018);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1019);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1020);
INSERT INTO "public"."sys_role_menu" VALUES (2, 104);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1021);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1022);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1023);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1024);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1025);
INSERT INTO "public"."sys_role_menu" VALUES (2, 105);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1026);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1027);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1028);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1029);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1030);
INSERT INTO "public"."sys_role_menu" VALUES (2, 106);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1031);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1032);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1033);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1034);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1035);
INSERT INTO "public"."sys_role_menu" VALUES (2, 107);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1036);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1037);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1038);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1039);
INSERT INTO "public"."sys_role_menu" VALUES (2, 108);
INSERT INTO "public"."sys_role_menu" VALUES (2, 500);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1040);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1041);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1042);
INSERT INTO "public"."sys_role_menu" VALUES (2, 501);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1043);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1044);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1045);
INSERT INTO "public"."sys_role_menu" VALUES (2, 2);
INSERT INTO "public"."sys_role_menu" VALUES (2, 109);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1046);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1047);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1048);
INSERT INTO "public"."sys_role_menu" VALUES (2, 110);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1049);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1050);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1051);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1052);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1053);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1054);
INSERT INTO "public"."sys_role_menu" VALUES (2, 111);
INSERT INTO "public"."sys_role_menu" VALUES (2, 112);
INSERT INTO "public"."sys_role_menu" VALUES (2, 113);
INSERT INTO "public"."sys_role_menu" VALUES (2, 3);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1055);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1056);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1058);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1057);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1059);
INSERT INTO "public"."sys_role_menu" VALUES (2, 1060);
INSERT INTO "public"."sys_role_menu" VALUES (2, 4);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_user";
CREATE TABLE "public"."sys_user" (
  "user_id" int8 NOT NULL DEFAULT nextval('user_id_seq'::regclass),
  "dept_id" int8,
  "user_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "nick_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "user_type" varchar(2) COLLATE "pg_catalog"."default" DEFAULT '00'::character varying,
  "email" varchar(50) COLLATE "pg_catalog"."default",
  "phonenumber" varchar(11) COLLATE "pg_catalog"."default",
  "sex" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "avatar" varchar(100) COLLATE "pg_catalog"."default",
  "password" varchar(100) COLLATE "pg_catalog"."default",
  "status" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "del_flag" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar,
  "login_ip" varchar(128) COLLATE "pg_catalog"."default",
  "login_date" timestamp(6),
  "create_by" varchar(64) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_by" varchar(64) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "remark" varchar(500) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."sys_user"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."sys_user"."dept_id" IS '部门ID';
COMMENT ON COLUMN "public"."sys_user"."user_name" IS '用户账号';
COMMENT ON COLUMN "public"."sys_user"."nick_name" IS '用户昵称';
COMMENT ON COLUMN "public"."sys_user"."user_type" IS '用户类型（00系统用户）';
COMMENT ON COLUMN "public"."sys_user"."email" IS '用户邮箱';
COMMENT ON COLUMN "public"."sys_user"."phonenumber" IS '手机号码';
COMMENT ON COLUMN "public"."sys_user"."sex" IS '用户性别（0男 1女 2未知）';
COMMENT ON COLUMN "public"."sys_user"."avatar" IS '头像地址';
COMMENT ON COLUMN "public"."sys_user"."password" IS '密码';
COMMENT ON COLUMN "public"."sys_user"."status" IS '帐号状态（0正常 1停用）';
COMMENT ON COLUMN "public"."sys_user"."del_flag" IS '删除标志（0代表存在 2代表删除）';
COMMENT ON COLUMN "public"."sys_user"."login_ip" IS '最后登录IP';
COMMENT ON COLUMN "public"."sys_user"."login_date" IS '最后登录时间';
COMMENT ON COLUMN "public"."sys_user"."create_by" IS '创建者';
COMMENT ON COLUMN "public"."sys_user"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."sys_user"."update_by" IS '更新者';
COMMENT ON COLUMN "public"."sys_user"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."sys_user"."remark" IS '备注';
COMMENT ON TABLE "public"."sys_user" IS '用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO "public"."sys_user" VALUES (4, 109, 'test', '测试', '00', '18988888888@qq.com', '18988888888', '0', '/profile/avatar/2021/06/07/a6a509ed-5819-4176-983f-b1f00aeca6f6.jpeg', '$2a$10$Vfg/bm74xDAGdUaGzC1.qeRB2LNQlPLtqQLavN8C.lOxAB1JpCSb.', '0', '2', '127.0.0.1', '2021-06-07 17:48:08.665', 'admin', '2021-06-07 15:45:54.323962', 'admin', '2021-06-07 20:56:52.780471', NULL);
INSERT INTO "public"."sys_user" VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '192.168.28.1', '2021-06-08 15:47:37.66', 'admin', '2021-06-04 13:57:09', '', '2021-06-08 15:47:44.871778', '管理员');
INSERT INTO "public"."sys_user" VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-06-04 13:57:09', 'admin', '2021-06-04 13:57:09', 'admin', '2021-06-08 15:48:05.803359', '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_user_post";
CREATE TABLE "public"."sys_user_post" (
  "user_id" int8 NOT NULL,
  "post_id" int8 NOT NULL
)
;
COMMENT ON COLUMN "public"."sys_user_post"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."sys_user_post"."post_id" IS '岗位ID';
COMMENT ON TABLE "public"."sys_user_post" IS '用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO "public"."sys_user_post" VALUES (1, 1);
INSERT INTO "public"."sys_user_post" VALUES (3, 1);
INSERT INTO "public"."sys_user_post" VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_user_role";
CREATE TABLE "public"."sys_user_role" (
  "user_id" int8 NOT NULL,
  "role_id" int8 NOT NULL
)
;
COMMENT ON COLUMN "public"."sys_user_role"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."sys_user_role"."role_id" IS '角色ID';
COMMENT ON TABLE "public"."sys_user_role" IS '用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO "public"."sys_user_role" VALUES (1, 1);
INSERT INTO "public"."sys_user_role" VALUES (3, 2);
INSERT INTO "public"."sys_user_role" VALUES (2, 2);

-- ----------------------------
-- View structure for view_self_table_columns
-- ----------------------------
DROP VIEW IF EXISTS "public"."view_self_table_columns";
CREATE VIEW "public"."view_self_table_columns" AS  SELECT columns.table_catalog,
    columns.table_schema,
    columns.table_name,
    columns.ordinal_position AS sort,
    columns.column_name,
    columns.data_type AS typename,
        CASE
            WHEN (((columns.is_nullable)::text = 'no'::text) AND (c.contype <> 'p'::"char")) THEN '1'::text
            ELSE NULL::text
        END AS is_required,
        CASE
            WHEN (c.contype = 'p'::"char") THEN '1'::text
            ELSE '0'::text
        END AS is_pk,
    COALESCE((columns.character_maximum_length)::integer, (columns.numeric_precision)::integer, '-1'::integer) AS length,
    columns.numeric_scale AS scale,
        CASE columns.is_nullable
            WHEN 'NO'::text THEN 0
            ELSE 1
        END AS cannull,
    columns.column_default AS defaultval,
        CASE
            WHEN ("position"((columns.column_default)::text, 'nextval'::text) > 0) THEN 1
            ELSE 0
        END AS isidentity,
        CASE
            WHEN ("position"((columns.column_default)::text, 'nextval'::text) > 0) THEN 1
            ELSE 0
        END AS is_increment,
    c.detext AS column_comment,
    c.typname AS column_type,
    c.contype,
    columns.ordinal_position
   FROM (information_schema.columns
     LEFT JOIN ( SELECT pg_database.datname,
            pg_get_userbyid(pg_class.relowner) AS tableowner,
            pg_ns.nspname,
            pg_class.relname,
            pg_attr.attname,
            pg_desc.description AS detext,
            pg_type.typname,
            pg_cons.contype
           FROM ((((((pg_class
             LEFT JOIN pg_attribute pg_attr ON ((pg_attr.attrelid = pg_class.oid)))
             LEFT JOIN pg_description pg_desc ON (((pg_desc.objoid = pg_attr.attrelid) AND (pg_desc.objsubid = pg_attr.attnum))))
             LEFT JOIN pg_namespace pg_ns ON ((pg_ns.oid = pg_class.relnamespace)))
             LEFT JOIN pg_database ON ((pg_class.relowner = pg_database.datdba)))
             LEFT JOIN pg_type ON ((pg_attr.atttypid = pg_type.oid)))
             LEFT JOIN ( SELECT pg_con.conname,
                    pg_con.connamespace,
                    pg_con.contype,
                    pg_con.condeferrable,
                    pg_con.condeferred,
                    pg_con.convalidated,
                    pg_con.conrelid,
                    pg_con.contypid,
                    pg_con.conindid,
                    pg_con.conparentid,
                    pg_con.confrelid,
                    pg_con.confupdtype,
                    pg_con.confdeltype,
                    pg_con.confmatchtype,
                    pg_con.conislocal,
                    pg_con.coninhcount,
                    pg_con.connoinherit,
                    pg_con.conkey,
                    pg_con.confkey,
                    pg_con.conpfeqop,
                    pg_con.conppeqop,
                    pg_con.conffeqop,
                    pg_con.conexclop,
                    pg_con.conbin,
                    pg_con.consrc,
                    unnest(pg_con.conkey) AS conkey_new
                   FROM pg_constraint pg_con) pg_cons ON (((pg_attr.attrelid = pg_class.oid) AND (pg_attr.attnum = pg_cons.conkey_new) AND (pg_cons.conrelid = pg_class.oid))))
          WHERE ((pg_attr.attnum > 0) AND (pg_attr.attrelid = pg_class.oid))) c ON ((((columns.table_catalog)::name = c.datname) AND ((columns.table_schema)::name = c.nspname) AND ((columns.table_name)::name = c.relname) AND ((columns.column_name)::name = c.attname))));

-- ----------------------------
-- View structure for view_self_table
-- ----------------------------
DROP VIEW IF EXISTS "public"."view_self_table";
CREATE VIEW "public"."view_self_table" AS  SELECT pg_database.datname AS table_catalog,
    pg_get_userbyid(c.relowner) AS tableowner,
    pg_ns.nspname AS table_schema,
    c.relname AS table_name,
    (obj_description(c.relfilenode, 'pg_class'::name))::character varying AS table_comment,
    now() AS create_time,
    now() AS update_time
   FROM ((pg_class c
     LEFT JOIN pg_namespace pg_ns ON ((pg_ns.oid = c.relnamespace)))
     LEFT JOIN pg_database ON ((c.relowner = pg_database.datdba)))
  WHERE (c.relname IN ( SELECT pg_tables.tablename
           FROM pg_tables));

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."column_id_seq"', 73, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."config_id_seq"', 6, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."dept_id_seq"', 113, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."dict_code_seq"', 31, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."dict_id_seq"', 13, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."info_id_seq"', 72, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."job_id_seq"', 6, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."job_log_id_seq"', 12, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."menu_id_seq"', 1064, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."notice_id_seq"', 5, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."oper_id_seq"', 210, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."post_id_seq"', 7, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."role_id_seq"', 7, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."table_id_seq"', 10, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."user_id_seq"', 6, true);

-- ----------------------------
-- Primary Key structure for table gen_table
-- ----------------------------
ALTER TABLE "public"."gen_table" ADD CONSTRAINT "gen_table_pkey" PRIMARY KEY ("table_id");

-- ----------------------------
-- Primary Key structure for table gen_table_column
-- ----------------------------
ALTER TABLE "public"."gen_table_column" ADD CONSTRAINT "gen_table_column_pkey" PRIMARY KEY ("column_id");

-- ----------------------------
-- Primary Key structure for table qrtz_blob_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_blob_triggers" ADD CONSTRAINT "qrtz_blob_triggers_pkey" PRIMARY KEY ("sched_name", "trigger_name", "trigger_group");

-- ----------------------------
-- Primary Key structure for table qrtz_calendars
-- ----------------------------
ALTER TABLE "public"."qrtz_calendars" ADD CONSTRAINT "qrtz_calendars_pkey" PRIMARY KEY ("sched_name", "calendar_name");

-- ----------------------------
-- Primary Key structure for table qrtz_cron_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_cron_triggers" ADD CONSTRAINT "qrtz_cron_triggers_pkey" PRIMARY KEY ("sched_name", "trigger_name", "trigger_group");

-- ----------------------------
-- Indexes structure for table qrtz_fired_triggers
-- ----------------------------
CREATE INDEX "idx_qrtz_ft_inst_job_req_rcvry" ON "public"."qrtz_fired_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "instance_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "requests_recovery" "pg_catalog"."bool_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_ft_j_g" ON "public"."qrtz_fired_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "job_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "job_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_ft_jg" ON "public"."qrtz_fired_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "job_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_ft_t_g" ON "public"."qrtz_fired_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_ft_tg" ON "public"."qrtz_fired_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_ft_trig_inst_name" ON "public"."qrtz_fired_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "instance_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table qrtz_fired_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_fired_triggers" ADD CONSTRAINT "qrtz_fired_triggers_pkey" PRIMARY KEY ("sched_name", "entry_id");

-- ----------------------------
-- Indexes structure for table qrtz_job_details
-- ----------------------------
CREATE INDEX "idx_qrtz_j_grp" ON "public"."qrtz_job_details" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "job_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_j_req_recovery" ON "public"."qrtz_job_details" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "requests_recovery" "pg_catalog"."bool_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table qrtz_job_details
-- ----------------------------
ALTER TABLE "public"."qrtz_job_details" ADD CONSTRAINT "qrtz_job_details_pkey" PRIMARY KEY ("sched_name", "job_name", "job_group");

-- ----------------------------
-- Primary Key structure for table qrtz_locks
-- ----------------------------
ALTER TABLE "public"."qrtz_locks" ADD CONSTRAINT "qrtz_locks_pkey" PRIMARY KEY ("sched_name", "lock_name");

-- ----------------------------
-- Primary Key structure for table qrtz_paused_trigger_grps
-- ----------------------------
ALTER TABLE "public"."qrtz_paused_trigger_grps" ADD CONSTRAINT "qrtz_paused_trigger_grps_pkey" PRIMARY KEY ("sched_name", "trigger_group");

-- ----------------------------
-- Primary Key structure for table qrtz_scheduler_state
-- ----------------------------
ALTER TABLE "public"."qrtz_scheduler_state" ADD CONSTRAINT "qrtz_scheduler_state_pkey" PRIMARY KEY ("sched_name", "instance_name");

-- ----------------------------
-- Primary Key structure for table qrtz_simple_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_simple_triggers" ADD CONSTRAINT "qrtz_simple_triggers_pkey" PRIMARY KEY ("sched_name", "trigger_name", "trigger_group");

-- ----------------------------
-- Primary Key structure for table qrtz_simprop_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_simprop_triggers" ADD CONSTRAINT "qrtz_simprop_triggers_pkey" PRIMARY KEY ("sched_name", "trigger_name", "trigger_group");

-- ----------------------------
-- Indexes structure for table qrtz_triggers
-- ----------------------------
CREATE INDEX "idx_qrtz_t_c" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "calendar_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_g" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_j" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "job_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "job_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_jg" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "job_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_n_g_state" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_state" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_n_state" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_state" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_next_fire_time" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "next_fire_time" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_nft_misfire" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "misfire_instr" "pg_catalog"."int2_ops" ASC NULLS LAST,
  "next_fire_time" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_nft_st" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_state" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "next_fire_time" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_nft_st_misfire" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "misfire_instr" "pg_catalog"."int2_ops" ASC NULLS LAST,
  "next_fire_time" "pg_catalog"."int8_ops" ASC NULLS LAST,
  "trigger_state" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_nft_st_misfire_grp" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "misfire_instr" "pg_catalog"."int2_ops" ASC NULLS LAST,
  "next_fire_time" "pg_catalog"."int8_ops" ASC NULLS LAST,
  "trigger_group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_state" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "idx_qrtz_t_state" ON "public"."qrtz_triggers" USING btree (
  "sched_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "trigger_state" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table qrtz_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_triggers" ADD CONSTRAINT "qrtz_triggers_pkey" PRIMARY KEY ("sched_name", "trigger_name", "trigger_group");

-- ----------------------------
-- Primary Key structure for table sys_config
-- ----------------------------
ALTER TABLE "public"."sys_config" ADD CONSTRAINT "sys_config_pkey" PRIMARY KEY ("config_id");

-- ----------------------------
-- Primary Key structure for table sys_dept
-- ----------------------------
ALTER TABLE "public"."sys_dept" ADD CONSTRAINT "sys_dept_pkey" PRIMARY KEY ("dept_id");

-- ----------------------------
-- Primary Key structure for table sys_dict_data
-- ----------------------------
ALTER TABLE "public"."sys_dict_data" ADD CONSTRAINT "sys_dict_data_pkey" PRIMARY KEY ("dict_code");

-- ----------------------------
-- Indexes structure for table sys_dict_type
-- ----------------------------
CREATE INDEX "dict_type" ON "public"."sys_dict_type" USING btree (
  "dict_type" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table sys_dict_type
-- ----------------------------
ALTER TABLE "public"."sys_dict_type" ADD CONSTRAINT "sys_dict_type_pkey" PRIMARY KEY ("dict_id");

-- ----------------------------
-- Primary Key structure for table sys_job
-- ----------------------------
ALTER TABLE "public"."sys_job" ADD CONSTRAINT "sys_job_pkey" PRIMARY KEY ("job_id", "job_name", "job_group");

-- ----------------------------
-- Primary Key structure for table sys_job_log
-- ----------------------------
ALTER TABLE "public"."sys_job_log" ADD CONSTRAINT "sys_job_log_pkey" PRIMARY KEY ("job_log_id");

-- ----------------------------
-- Primary Key structure for table sys_logininfor
-- ----------------------------
ALTER TABLE "public"."sys_logininfor" ADD CONSTRAINT "sys_logininfor_pkey" PRIMARY KEY ("info_id");

-- ----------------------------
-- Primary Key structure for table sys_menu
-- ----------------------------
ALTER TABLE "public"."sys_menu" ADD CONSTRAINT "sys_menu_pkey" PRIMARY KEY ("menu_id");

-- ----------------------------
-- Primary Key structure for table sys_notice
-- ----------------------------
ALTER TABLE "public"."sys_notice" ADD CONSTRAINT "sys_notice_pkey" PRIMARY KEY ("notice_id");

-- ----------------------------
-- Primary Key structure for table sys_oper_log
-- ----------------------------
ALTER TABLE "public"."sys_oper_log" ADD CONSTRAINT "sys_oper_log_pkey" PRIMARY KEY ("oper_id");

-- ----------------------------
-- Primary Key structure for table sys_post
-- ----------------------------
ALTER TABLE "public"."sys_post" ADD CONSTRAINT "sys_post_pkey" PRIMARY KEY ("post_id");

-- ----------------------------
-- Primary Key structure for table sys_role
-- ----------------------------
ALTER TABLE "public"."sys_role" ADD CONSTRAINT "sys_role_pkey" PRIMARY KEY ("role_id");

-- ----------------------------
-- Primary Key structure for table sys_role_dept
-- ----------------------------
ALTER TABLE "public"."sys_role_dept" ADD CONSTRAINT "sys_role_dept_pkey" PRIMARY KEY ("role_id", "dept_id");

-- ----------------------------
-- Primary Key structure for table sys_role_menu
-- ----------------------------
ALTER TABLE "public"."sys_role_menu" ADD CONSTRAINT "sys_role_menu_pkey" PRIMARY KEY ("role_id", "menu_id");

-- ----------------------------
-- Primary Key structure for table sys_user
-- ----------------------------
ALTER TABLE "public"."sys_user" ADD CONSTRAINT "sys_user_pkey" PRIMARY KEY ("user_id");

-- ----------------------------
-- Primary Key structure for table sys_user_post
-- ----------------------------
ALTER TABLE "public"."sys_user_post" ADD CONSTRAINT "sys_user_post_pkey" PRIMARY KEY ("user_id", "post_id");

-- ----------------------------
-- Primary Key structure for table sys_user_role
-- ----------------------------
ALTER TABLE "public"."sys_user_role" ADD CONSTRAINT "sys_user_role_pkey" PRIMARY KEY ("user_id", "role_id");

-- ----------------------------
-- Foreign Keys structure for table qrtz_blob_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_blob_triggers" ADD CONSTRAINT "qrtz_blob_triggers_sched_name_fkey" FOREIGN KEY ("sched_name", "trigger_name", "trigger_group") REFERENCES "public"."qrtz_triggers" ("sched_name", "trigger_name", "trigger_group") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table qrtz_cron_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_cron_triggers" ADD CONSTRAINT "qrtz_cron_triggers_sched_name_fkey" FOREIGN KEY ("sched_name", "trigger_name", "trigger_group") REFERENCES "public"."qrtz_triggers" ("sched_name", "trigger_name", "trigger_group") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table qrtz_simple_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_simple_triggers" ADD CONSTRAINT "qrtz_simple_triggers_sched_name_fkey" FOREIGN KEY ("sched_name", "trigger_name", "trigger_group") REFERENCES "public"."qrtz_triggers" ("sched_name", "trigger_name", "trigger_group") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table qrtz_simprop_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_simprop_triggers" ADD CONSTRAINT "qrtz_simprop_triggers_sched_name_fkey" FOREIGN KEY ("sched_name", "trigger_name", "trigger_group") REFERENCES "public"."qrtz_triggers" ("sched_name", "trigger_name", "trigger_group") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table qrtz_triggers
-- ----------------------------
ALTER TABLE "public"."qrtz_triggers" ADD CONSTRAINT "qrtz_triggers_sched_name_fkey" FOREIGN KEY ("sched_name", "job_name", "job_group") REFERENCES "public"."qrtz_job_details" ("sched_name", "job_name", "job_group") ON DELETE NO ACTION ON UPDATE NO ACTION;
